version = "0.1.0"

plugins {
    kotlin("jvm") version "1.9.25"
    kotlin("plugin.serialization") version "1.9.25"
    `jvm-test-suite`

    // Add the Shadow plugin for fat jar generation
    id("com.gradleup.shadow") version "8.3.5"

    // Apply the application plugin to add support for building a CLI application in Java.
    application
}

repositories {
    mavenCentral()
    maven {
        setUrl("https://plugins.gradle.org/m2/")
    }
    maven {
        // This repo is needed for Argon2. Won't be needed once keznacl is moved out of the source
        // tree.
        setUrl("https://maven.scijava.org/content/repositories/public/")
    }
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.3")

    // For Apache's awesome FileUtils
    implementation("commons-io:commons-io:2.16.1")

    // Needed by keznacl
    implementation("com.github.alphazero:Blake2b:bbf094983c")
    implementation("org.purejava:tweetnacl-java:1.1.2")
    implementation("de.mkammerer:argon2-jvm:2.11")

    // For parsing the server's config file
    implementation("io.hotmoka:toml4j:0.7.3")

    // Because we use PostgreSQL for the database
    implementation("org.postgresql:postgresql:42.7.3")

    // For the DNSHandler class
    implementation("org.minidns:minidns-hla:1.0.5")

    // Needed by libkeycard for RandomID ULID backend
    implementation("com.github.f4b6a3:ulid-creator:5.2.3")

    // JUnit 5 for testing
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.10.2")

    // NOTE: for some bizarre Gradle reason, if the SQLite drive is listed before the Postgres
    // driver, the resulting shadowjar does not work because the Postgres driver is missing.
    // *facepalm*

    // Needed by libmensago for KDB
    implementation("org.xerial:sqlite-jdbc:3.46.0.0")

    // Get rid of SLF4J warnings -- also needed for libmensago
    implementation("org.slf4j:slf4j-simple:2.0.13")
}

testing {
    suites {
        @Suppress("UnstableApiUsage")
        val integrationTest by registering(JvmTestSuite::class) {
            dependencies {
                implementation(project())
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.3")

                // TOML and Postgres support because the integration tests directly interact with
                // the local server config and database
                implementation("com.moandjiezana.toml:toml4j:0.7.2")
                implementation("org.postgresql:postgresql:42.7.3")

                // Use FileUtils again for easy recursive deletion of directories and other handy
                // features
                implementation("commons-io:commons-io:2.13.0")

                // For DNS testing
                implementation("org.minidns:minidns-hla:1.0.4")
            }
        }
    }
}


java {
    toolchain {
        // We need JVM 21+ because we use virtual threads for concurrency
        languageVersion.set(JavaLanguageVersion.of(21))
    }
}

application {
    // Define the main class for the application.
    mainClass.set("mensagod.AppKt")
}

tasks.named<Test>("test") {
    useJUnitPlatform()
}

tasks.withType<Jar>().configureEach {
    archiveBaseName.set(rootProject.name)
    archiveVersion.set(project.version.toString())
    archiveClassifier.set("")

    manifest {
        attributes(
            mapOf(
                "Main-Class" to application.mainClass,
                "Implementation-Title" to project.name,
                "Implementation-Version" to project.version
            )
        )
    }
}
