package libmensago.db

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class KDBTest {

    @Test
    fun connectionTests() {
        val db = KDB(SqliteConfig(":memory:")).connect().getOrThrow()

        assert(db.isConnected())
        db.disconnect()
        assert(!db.isConnected())
        db.connect().getOrThrow()
        assert(db.isConnected())
    }

    @Test
    fun execute() {
        val db = KDB(SqliteConfig(":memory:")).connect().getOrThrow()

        db.execute(
            """CREATE TABLE 'testtable' (
                    'rowid' INTEGER PRIMARY KEY AUTOINCREMENT,
                    'wid' TEXT NOT NULL UNIQUE, 'userid' TEXT);"""
        ).getOrThrow()
        db.execute("""INSERT INTO testtable(wid,userid) VALUES('foo', 'bar');""").getOrThrow()
        assertNotNull(db.execute("CREATE ;").exceptionOrNull())

        val conn = db.getConnection()!!
        val stmt = conn.prepareStatement("SELECT wid,userid FROM 'testtable';")
        val rs = stmt.executeQuery()
        assert(rs.next())
        assertEquals("foo", rs.getString(1))
        assertEquals("bar", rs.getString(2))
    }

    @Test
    fun queryAndExists() {
        val db = KDB(SqliteConfig(":memory:")).connect().getOrThrow()

        db.execute(
            """CREATE TABLE 'testtable' (
                    'rowid' INTEGER PRIMARY KEY AUTOINCREMENT,
                    'wid' TEXT NOT NULL UNIQUE, 'userid' TEXT);"""
        ).getOrThrow()
        db.execute("""INSERT INTO testtable(wid,userid) VALUES('foo', 'bar');""").getOrThrow()


        val rs = db.query("SELECT wid,userid FROM 'testtable';").getOrThrow()
        assert(rs.next())
        assertEquals("foo", rs.getString(1))
        assertEquals("bar", rs.getString(2))

        assert(db.exists("SELECT wid,userid FROM 'testtable';").getOrThrow())
        assertFalse(db.exists("SELECT wid FROM 'testtable' WHERE wid='bar';").getOrThrow())
    }

    @Test
    fun batch() {
        val db = KDB(SqliteConfig(":memory:")).connect().getOrThrow()

        db.execute(
            """CREATE TABLE 'testtable' (
                    'rowid' INTEGER PRIMARY KEY AUTOINCREMENT,
                    'wid' TEXT NOT NULL UNIQUE, 'userid' TEXT);"""
        ).getOrThrow()

        db.add("""INSERT INTO testtable(wid,userid) VALUES('foo1', 'bar1');""")
            ?.let { throw it }
        db.add("""INSERT INTO testtable(wid,userid) VALUES('foo2', 'bar2');""")
            ?.let { throw it }
        db.add("""INSERT INTO testtable(wid,userid) VALUES('foo3', 'bar3');""")
            ?.let { throw it }
        db.executeBatch()?.let { throw it }

        val rs = db.query("SELECT count(*) FROM 'testtable';").getOrThrow()
        assert(rs.next())
        assertEquals(3, rs.getInt(1))
    }
}