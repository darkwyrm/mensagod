package mensagod

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test

class StartupOptionsTest {

    @Test
    fun basicTests() {
        StartupOptions.init(arrayOf("--help"))

        assertFalse(StartupOptions.testMode)
        assertFalse(StartupOptions.debugMode)
        assertEquals(0, StartupOptions.init(arrayOf("--test")))
        assert(StartupOptions.debugMode)
        assert(StartupOptions.testMode)

        assertEquals(0, StartupOptions.init(arrayOf("--debug")))
        assert(StartupOptions.debugMode)
        assertFalse(StartupOptions.testMode)

        assert(StartupOptions.init(arrayOf("--weakhashes")) != 0)
        assert(StartupOptions.init(arrayOf("--debug", "--weakhashes")) != 0)
        assertEquals(0, StartupOptions.init(arrayOf("--test", "--weakhashes")))
        assert(StartupOptions.debugMode)
        assert(StartupOptions.testMode)
        assert(StartupOptions.useWeakPasswordHashing)
    }
}