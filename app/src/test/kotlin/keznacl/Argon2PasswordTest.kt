package keznacl

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class Argon2PasswordTest {

    private val weakTestHash = "\$argon2id\$v=19\$m=16384,t=1,p=1\$z48dw25lKtFmfLw6HTvR/g" +
            "\$TSXxBPLhZ1U2B+mBxOyw63WlwpUpwpfihRTEzTMfa44"
    private val weak2iHash = "\$argon2i\$v=19\$m=16384,t=1,p=1\$86jbEKB3WO0Nr8okRL9/MA" +
            "\$X0J81SApJRz44CX791lme+mfH8hy3NUMGFDOfZdIlak"
    private val weak2dHash = "\$argon2d\$v=19\$m=16384,t=1,p=1\$/0l+lSUhmlySJtm3Xg54gw" +
            "\$GX+3qldzdTISDoB3TKtjhXD2S5jgFbkhbzU7FLdqslc"

    @Test
    fun basicTests() {
        val hasher = Argon2idPassword()
        hasher.updateHash("MyS3cretPassw*rd").getOrThrow()
        assert(hasher.verify("MyS3cretPassw*rd"))
    }

    @Test
    fun passwordSync() {
        val firstHasher = Argon2idPassword()
        firstHasher.run {
            setIterations(1)
            setMemory(16384)
            setThreads(1)
        }
        firstHasher.updateHash("MyS3cretPassw*rd")

        val secondHasher = Argon2idPassword()
        secondHasher.run {
            setIterations(1)
            setMemory(16384)
            setThreads(1)
            setSalt(firstHasher.getSalt())
        }
        secondHasher.updateHash("MyS3cretPassw*rd")
        assertEquals(firstHasher.getHash(), secondHasher.getHash())
    }

    @Test
    fun setFromHash() {
        val hasher = argonPassFromHash(weakTestHash).getOrThrow()
        assertEquals(weakTestHash, hasher.getHash())
        assertEquals("z48dw25lKtFmfLw6HTvR/g", hasher.getSalt())
        assertEquals("m=16384,t=1,p=1", hasher.getParameters())
        assertEquals(1, hasher.getIterations())
        assertEquals(16384, hasher.getMemory())
        assertEquals(1, hasher.getThreads())
        assertEquals(19, hasher.getVersion())
    }

    @Test
    fun setFromInfo() {
        val info = PasswordInfo(
            "ARGON2ID",
            "z48dw25lKtFmfLw6HTvR/g",
            "m=16384,t=1,p=2"
        )
        val hasher = passhasherForInfo(info).getOrThrow() as Argon2idPassword
        assertEquals(16384, hasher.getMemory())
        assertEquals(1, hasher.getIterations())
        assertEquals(2, hasher.getThreads())

        assert(passhasherForInfo(PasswordInfo("BADALGO")).isFailure)
    }

    @Test
    fun setStrength() {
        val hasher = Argon2idPassword()
        hasher.run {
            setIterations(5)
            setMemory(16384)
            setThreads(2)
            setStrength(HashStrength.Basic)
        }
        assertEquals(0x100_000, hasher.getMemory())
        assertEquals(10, hasher.getIterations())
        assertEquals(4, hasher.getThreads())
    }

    @Test
    fun validateInfo() {
        assertNull(
            validatePasswordInfo(
                PasswordInfo(
                    "ARGON2ID",
                    "z48dw25lKtFmfLw6HTvR/g",
                    "m=16384,t=1,p=1"
                )
            )
        )

        assertNotNull(
            validatePasswordInfo(
                PasswordInfo(
                    "BADALGO",
                    "z48dw25lKtFmfLw6HTvR/g",
                    "m=16384,t=1,p=1"
                )
            )
        )

        assertNotNull(
            validatePasswordInfo(
                PasswordInfo(
                    "ARGON2ID",
                    "",
                    "m=16384,t=1,p=1"
                )
            )
        )

        assertNotNull(
            validatePasswordInfo(
                PasswordInfo(
                    "ARGON2ID",
                    "z48dw25lKtFmfLw6HTvR/g",
                    ""
                )
            )
        )

        assertNotNull(
            validatePasswordInfo(
                PasswordInfo(
                    "ARGON2ID",
                    "z48dw25lKtFmfLw6HTvR/g",
                    "m=a,t=1,p=1"
                )
            )
        )

        assertNotNull(
            validatePasswordInfo(
                PasswordInfo(
                    "ARGON2ID",
                    "z48dw25lKtFmfLw6HTvR/g",
                    "m=16384,t=a,p=1"
                )
            )
        )

        assertNotNull(
            validatePasswordInfo(
                PasswordInfo(
                    "ARGON2ID",
                    "z48dw25lKtFmfLw6HTvR/g",
                    "m=16384,t=1,p=a"
                )
            )
        )
    }

    @Test
    fun compatibility() {
        val hasher = Argon2idPassword()
        val hash = hasher.updateHash("this is a test").getOrThrow()
        val hasher2 = Argon2idPassword()
        hasher2.setFromHash(hash)
        assert(hasher2.verify("this is a test"))
    }

    @Test
    fun lintRemoval() {
        val ihasher = Argon2iPassword()
        ihasher.run {
            setIterations(1)
            setMemory(16384)
            setThreads(1)
        }
        assertNull(ihasher.setFromHash(weak2iHash))

        val dhasher = Argon2dPassword()
        dhasher.run {
            setIterations(1)
            setMemory(16384)
            setThreads(1)
        }
        assertNull(dhasher.setFromHash(weak2dHash))
    }
}