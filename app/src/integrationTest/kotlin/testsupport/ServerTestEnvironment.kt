package testsupport

import keznacl.CryptoString
import keznacl.EncryptionPair
import keznacl.SigningPair
import libkeycard.*
import libmensago.*
import libmensago.db.PGConfig
import libmensago.resolver.KCResolver
import mensagod.*
import mensagod.dbcmds.*
import org.apache.commons.io.FileUtils
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.nio.file.Paths

/*
    Tests are written such that they (a) don't care about how they connect to the database, so
    long as they can and (b) don't care where in the filesystem their files are stored so long
    as they have a place to work in standard setup, so why not abstract away the details.

    Some tests will need mock server-side data and some will not. This needs to support both.
 */

const val SETUP_TEST_FILESYSTEM: Int = 1
const val SETUP_TEST_DATABASE: Int = 2
const val SETUP_TEST_ADMIN: Int = 3
const val SETUP_TEST_USER: Int = 4
const val SETUP_TEST_ADMIN_KEYCARD: Int = 6
const val SETUP_TEST_BOTH_KEYCARDS: Int = 7

/**
 * This support class is responsible for setting up the server side of anenvironment for integration
 * tests.
 *
 * NOTE: If a test requires database setup, the user account utilized to connect to the database
 * must have CREATEDB privileges to create a database for each test. This is to permit
 * parallelization and prevent test from stepping on one another's toes.
 */
class ServerTestEnvironment(val testName: String) {
    val serverconfig: ServerConfig = ServerConfig.load().getOrThrow()
    private val dbconfig: PGConfig = PGConfig(
        serverconfig.getString("database.user")!!,
        serverconfig.getString("database.password")!!,
        serverconfig.getString("database.host")!!,
        serverconfig.getInteger("database.port")!!,
        testName
    )
    private var db: PGConn? = null
    private var serverPrimary: SigningPair? = null
    private var serverEncryption: EncryptionPair? = null
    private var serverCard: Keycard? = null

    val testPath: String = Paths.get("build", "testfiles", testName).toAbsolutePath()
        .toString()

    val adminProfile = createAdminProfileData()
    val userProfile = createUserProfileData()

    /**
     * Provisions the test environment based on the configuration set
     */
    fun provision(provisionLevel: Int): ServerTestEnvironment {
        setupTestBase()
        if (provisionLevel >= SETUP_TEST_DATABASE)
            initDatabase()
        if (provisionLevel >= SETUP_TEST_ADMIN)
            regAdmin()
        if (provisionLevel >= SETUP_TEST_USER)
            regUser()
        if (provisionLevel >= SETUP_TEST_ADMIN_KEYCARD)
            setupKeycard(adminProfile)
        if (provisionLevel >= SETUP_TEST_BOTH_KEYCARDS)
            setupKeycard(userProfile)

        return this
    }

    /**
     * Shuts down the test environment. This is primarily to close any database connections.
     */
    fun done() {
        db?.disconnect()
    }

    /**
     * Returns a connection to the database. This will throw an exception if the database connection
     * has not been initialized.
     */
    fun getDB(): PGConn {
        return db ?: throw TestFailureException("Database not initialized")
    }

    /**
     * Returns the server's current primary signing keypair or throws an exception. This call
     * requires database initialization.
     */
    fun serverPrimaryPair(): SigningPair {
        return serverPrimary ?: throw TestFailureException("Database not initialized")
    }

    /**
     * Returns the server's current encryption keypair or throws an exception. This call requires
     * database initialization.
     */
    fun serverEncryptionPair(): EncryptionPair {
        return serverEncryption ?: throw TestFailureException("Database not initialized")
    }

    /**
     * Provisions the integration test folder hierarchy and environment baseline. The test root
     * contains one subfolder, topdir, which is the server's workspace data folder. This also
     * provides other filesystem-related setup, such as initializing logging, LocalFS, and the
     * mock DNS handler.
     */
    private fun setupTestBase() {
        val topdir = Paths.get(testPath, "topdir")
        val topdirFile = topdir.toFile()
        if (topdirFile.exists())
            FileUtils.cleanDirectory(topdirFile)
        else
            topdirFile.mkdirs()

        initLogging(Paths.get(testPath, "log.txt"), true)
        LocalFS.initialize(topdir.toString())?.let { throw it }
        gServerDomain = Domain.fromString(serverconfig.getString("global.domain"))!!
        gServerAddress = WAddress.fromParts(gServerDevID, gServerDomain)
    }

    /**
     * Ensures that the database to be used by the test exists
     */
    private fun ensureDB() {
        serverconfig.setValue("database.name", testName)
        val dbsetupcfg = PGConfig(
            serverconfig.getString("database.user")!!,
            serverconfig.getString("database.password")!!,
            serverconfig.getString("database.host")!!,
            serverconfig.getInteger("database.port")!!,
            "postgres"
        )
        // Swallow the exception if we fail to create the database. It simplifies the logic a lot
        // as Postgres doesn't have a CREATE IF NOT EXISTS syntax for database creation. If we fail
        // to create the database for other reasons, the test will fail later on.
        val pgConn = PGConn(dbsetupcfg)
        kotlin.runCatching {
            pgConn.execute("DROP DATABASE $testName")
        }.getOrElse {
            println("Error dropping database $testName: $it")
        }
        kotlin.runCatching {
            pgConn.execute("CREATE DATABASE $testName OWNER ${dbsetupcfg.user}")
        }.getOrElse {
            println("Error creating database $testName: $it")
        }
        // This call must go after the database setup. The FakeDNSHandler class creates a connection
        // to the database and doesn't destroy it until the process ends.
        KCResolver.dns = FakeDNSHandler(serverconfig.connectToDB().getOrThrow())
    }

    /**
     * Provisions a database for the integration test. The database is populated with all tables
     * needed and adds basic data to the database as if setup had been run. It also rotates the org
     * keycard so that there are two entries.
     */
    private fun initDatabase() {
        ensureDB()
        resetDB(serverconfig.getDBConfig())
        db = PGConn(dbconfig)
        DBConn.initialize(serverconfig)?.let { throw it }

        val initialOSPair = SigningPair.fromStrings(
            "ED25519:r#r*RiXIN-0n)BzP3bv`LA&t4LFEQNF0Q@\$N~RF*",
            "ED25519:{UNQmjYhz<(-ikOBYoEQpXPt<irxUF*nq25PoW=_"
        ).getOrThrow()
        val initialOEPair = EncryptionPair.fromStrings(
            "CURVE25519:SNhj2K`hgBd8>G>lW\$!pXiM7S-B!Fbd9jT2&{{Az",
            "CURVE25519:WSHgOhi+bg=<bO^4UoJGF-z9`+TBN{ds?7RZ;w3o"
        ).getOrThrow()

        val rootEntry = OrgEntry().run {
            setFieldInteger("Index", 1)
            setField("Name", "Example, Inc.")
            setField(
                "Contact-Admin",
                "ae406c5e-2673-4d3e-af20-91325d9623ca/example.com"
            )
            setField("Language", "en")
            setField("Domain", "example.com")
            setField("Primary-Verification-Key", initialOSPair.pubKey.toString())
            setField("Encryption-Key", initialOEPair.pubKey.toString())

            isDataCompliant()?.let { throw it }
            hash()?.let { throw it }
            sign("Organization-Signature", initialOSPair)?.let { throw it }
            verifySignature("Organization-Signature", initialOSPair).getOrThrow()
            isCompliant()?.let { throw it }
            this
        }

        serverCard = Keycard.new("Organization")!!
        serverCard!!.entries.add(rootEntry)

        db!!.execute(
            "INSERT INTO keycards(owner,creationtime,index,entry,fingerprint) " +
                    "VALUES('organization',?,?,?,?);",
            rootEntry.getFieldString("Timestamp")!!,
            rootEntry.getFieldInteger("Index")!!,
            rootEntry.getFullText(null).getOrThrow(),
            rootEntry.getAuthString("Hash")!!
        )

        val keys = serverCard!!.chain(initialOSPair, 365).getOrThrow()
        keys as OrgKeySet
        val newEntry = serverCard!!.entries[1]

        serverPrimary = keys.getSPair()
        serverEncryption = keys.getEPair()

        db!!.execute(
            "INSERT INTO keycards(owner,creationtime,index,entry,fingerprint) " +
                    "VALUES('organization',?,?,?,?);",
            newEntry.getFieldString("Timestamp")!!,
            newEntry.getFieldInteger("Index")!!,
            newEntry.getFullText(null).getOrThrow(),
            newEntry.getAuthString("Hash")!!
        )

        db!!.execute(
            "INSERT INTO orgkeys(creationtime,pubkey,privkey,purpose,fingerprint) " +
                    "VALUES(?,?,?,'encrypt',?);",
            newEntry.getFieldString("Timestamp")!!,
            keys.getEPair().pubKey,
            keys.getEPair().privKey,
            keys.getEPair().getPublicHash().getOrThrow()
        )

        db!!.execute(
            "INSERT INTO orgkeys(creationtime,pubkey,privkey,purpose,fingerprint) " +
                    "VALUES(?,?,?,'sign',?);",
            newEntry.getFieldString("Timestamp")!!,
            keys.getSPair().pubKey,
            keys.getSPair().privKey,
            keys.getSPair().getPublicHash().getOrThrow()
        )

        db!!.execute(
            "INSERT INTO orgkeys(creationtime,pubkey,privkey,purpose,fingerprint) " +
                    "VALUES(?,?,?,'altsign',?);",
            newEntry.getFieldString("Timestamp")!!,
            initialOSPair.pubKey,
            initialOSPair.privKey,
            initialOSPair.getPublicHash().getOrThrow()
        )
        // Preregister the admin account
        db!!.execute(
            "INSERT INTO prereg(wid,uid,domain,regcode) VALUES(?,?,?,?)",
            adminProfile.wid, "admin", "example.com", ADMIN_PROFILE_DATA["reghash"]!!
        )

        // Forward abuse and support to admin
        db!!.execute(
            "INSERT INTO workspaces(wid,uid,domain,password,passtype,status,wtype) " +
                    "VALUES(?,'abuse','example.com','-','','active','alias')",
            adminProfile.wid
        )
        db!!.execute(
            "INSERT INTO workspaces(wid,uid,domain,password,passtype,status,wtype) " +
                    "VALUES(?,'support','example.com','-','','active','alias')",
            adminProfile.wid
        )
    }

    /**
     * Registers the admin user and the admin's first device
     */
    private fun regAdmin() {
        val fakeInfo = CryptoString.fromString("XSALSA20:ABCDEFG1234567890")!!
        val db = DBConn()
        addWorkspace(
            db, adminProfile.wid, adminProfile.uid, gServerDomain,
            adminProfile.passhash,
            "argon2id", "anXvadxtNJAYa2cUQFqKSQ", "m=65536,t=2,p=1",
            WorkspaceStatus.Active, WorkspaceType.Individual
        )?.let { throw it }
        addDevice(
            db,
            adminProfile.wid,
            adminProfile.devid,
            adminProfile.devpair.pubKey,
            fakeInfo,
            DeviceStatus.Registered
        )?.let { throw it }
        deletePrereg(db, WAddress.fromParts(adminProfile.wid, gServerDomain))?.let { throw it }
        db.disconnect()
    }

    /**
     * Registers a user and their first device
     */
    private fun regUser() {
        val db = DBConn()
        preregWorkspace(
            db, userProfile.wid, userProfile.uid, gServerDomain,
            userProfile.passhash
        )?.let { throw it }
        LocalFS.get().entry(MServerPath("/ ${userProfile.wid}")).makeDirectory()

        val fakeInfo = CryptoString.fromString("XSALSA20:ABCDEFG1234567890")!!
        addWorkspace(
            db, userProfile.wid, userProfile.uid, gServerDomain,
            userProfile.passhash,
            "argon2id", "anXvadxtNJAYa2cUQFqKSQ", "m=65536,t=2,p=1",
            WorkspaceStatus.Active, WorkspaceType.Individual
        )?.let { throw it }
        addDevice(
            db,
            userProfile.wid,
            userProfile.devid,
            userProfile.devpair.pubKey,
            fakeInfo,
            DeviceStatus.Registered
        )?.let { throw it }
        deletePrereg(db, WAddress.fromParts(userProfile.wid, gServerDomain))?.let { throw it }
        db.disconnect()
    }

    /**
     * Sets up keycards for both the admin and the test user
     */
    private fun setupKeycard(profile: TestProfileData) {
        val db = DBConn()

        val card = profile.keycard
        with(card.current!!) {
            isDataCompliant()?.let { throw it }
            sign("Organization-Signature", serverPrimary!!)?.let { throw it }
            addAuthString(
                "Previous-Hash",
                serverCard!!.current!!.getAuthString("Hash")!!
            )?.let { throw it }
            hash()?.let { throw it }
            sign("User-Signature", profile.crsigning)?.let { throw it }
            isCompliant()?.let { throw it }
            addEntry(db, this)?.let { throw it }
        }

        val newKeys = card.chain(profile.crsigning).getOrThrow()
        newKeys as UserKeySet
        with(card.current!!) {
            sign("Organization-Signature", serverPrimary!!)?.let { throw it }
            hash()?.let { throw it }
            sign("User-Signature", profile.crsigning)?.let { throw it }
            isCompliant()?.let { throw it }
            addEntry(db, this)?.let { throw it }
        }
        assert(card.verify().getOrThrow())

        profile.data["crsigning"] = newKeys.getCRSPair()
        profile.data["crencryption"] = newKeys.getCREPair()
        profile.data["signing"] = newKeys.getSPair()
        profile.data["encryption"] = newKeys.getEPair()

        db.disconnect()
    }
}


class ServerEnvironmentTest {
    @Test
    fun testBaseSetup() {
        val env = ServerTestEnvironment("servertestenv_fs").provision(SETUP_TEST_FILESYSTEM)

        val lfs = LocalFS.get()
        val rootPath = MServerPath()
        assertEquals(
            lfs.convertToLocal(rootPath).toString(),
            Paths.get(env.testPath, "topdir").toString()
        )
        assert(rootPath.toHandle().exists().getOrThrow())

        val exampleIPs = KCResolver.dns
            .lookupA("example.com")
            .getOrThrow()
        assertEquals(1, exampleIPs.size)
        assertEquals("/127.0.0.1", exampleIPs[0].toString())

        val oldLevel = getLogLevel()
        setLogLevel(LOG_INFO)
        logInfo("${env.testName} log test entry")
        setLogLevel(oldLevel)
    }

    @Test
    fun testBaseDBSetup() {
        ServerTestEnvironment("servertestenv_basedb")
            .provision(SETUP_TEST_DATABASE)
    }


    @Test
    fun testKeycardSetup() {
        ServerTestEnvironment("servertestenv_keycard")
            .provision(SETUP_TEST_BOTH_KEYCARDS)
    }
}
