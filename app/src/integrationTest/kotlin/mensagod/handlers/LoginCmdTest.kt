package mensagod.handlers

import keznacl.CryptoString
import keznacl.EncryptionKey
import keznacl.EncryptionPair
import keznacl.SHAPassword
import kotlinx.serialization.json.Json
import libkeycard.RandomID
import libkeycard.Timestamp
import libmensago.*
import mensagod.*
import mensagod.dbcmds.getSyncRecords
import mensagod.dbcmds.resetPassword
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import testsupport.*
import java.net.InetAddress
import java.net.Socket

class LoginCmdTest {

    @Test
    fun deviceTest() {
        val env = ServerTestEnvironment("handlers_device")
            .provision(SETUP_TEST_ADMIN_KEYCARD)

        // Actually create some fake device info for the test
        val devInfo = DeviceInfo(
            env.adminProfile.devid,
            env.adminProfile.devpair.pubKey,
            env.adminProfile.devpair.privKey,
            mutableMapOf(
                "Name" to "dellxps",
                "User" to "Owner",
                "OS" to "Ubuntu 22.04",
                "Device-ID" to env.adminProfile.devid.toString(),
                "Device-Key" to env.adminProfile.devpair.pubKey.toString(),
                "Timestamp" to Timestamp().toString(),
            )
        )
        val userKey = env.adminProfile.encryption
        val encInfo = devInfo.encryptAttributes(userKey).getOrThrow()

        // Test Case #1: Successfully complete first device auth phase
        CommandTest(
            "device.1",
            SessionState(
                ClientRequest(
                    "DEVICE", mutableMapOf(
                        "Device-ID" to env.adminProfile.devid.toString(),
                        "Device-Key" to env.adminProfile.devpair.pubKey.toString(),
                        "Key-Hash" to env.adminProfile.devpair.getPublicHash().getOrThrow()
                            .toString(),
                        "Device-Info" to encInfo.toString(),
                    )
                ), gAdminProfileData.wid,
                LoginState.AwaitingDeviceID
            ), ::commandDevice
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            var response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(100)
            response.checkFields(listOf(Pair("Challenge", true)))

            // The challenge from the server is expected to be Base85-encoded random bytes that are
            // encrypted into a CryptoString. This means we decrypt the challenge and send the resulting
            // decrypted string back to the server as proof of device identity.

            val challStr = CryptoString.fromString(response.data["Challenge"]!!)!!
            val challDecrypted = env.adminProfile.devpair
                .decrypt(challStr).getOrThrow()
                .decodeToString()

            val req = ClientRequest(
                "DEVICE", mutableMapOf(
                    "Device-ID" to env.adminProfile.devid.toString(),
                    "Device-Key" to env.adminProfile.devpair.pubKey.toString(),
                    "Key-Hash" to env.adminProfile.devpair.getPublicHash().getOrThrow().toString(),
                    "Device-Info" to encInfo.toString(),
                    "Response" to challDecrypted,
                )
            )
            req.send(socket.getOutputStream())
            response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(200)

            // NOTE: we don't check for the change in server path in the session state here because
            // the test's server thread gets its own copy of the session state and, thus, our copy
            // here doesn't get modified. The condition is tested in the libmensago integration
            // tests.
        }.run()
        val db = DBConn()

        // Test Case #2: Successfully complete second device auth phase
        val devid2 = RandomID.fromString("35ee6e0d-add8-49ea-a70a-edd0b53db3e8")!!
        val dev2pair = EncryptionPair.fromStrings(
            "CURVE25519:#Ow)+4|Q%=5GG+o1Hd+lEjCTMhhY!yVswA7\$5%5w",
            "CURVE25519:Uah9%mzlehrI)UxPiNoc%4vJcBw4%jXyxAm6)HWd"
        ).getOrThrow()
        val devInfo2 = DeviceInfo(
            devid2, dev2pair.pubKey, dev2pair.privKey, mutableMapOf(
                "Device Name" to "myframework",
                "User Name" to "Owner",
                "OS" to "Windows 10 Pro",
                "Device ID" to devid2.toString(),
                "Device Key" to dev2pair.pubKey.toString(),
                "Timestamp" to Timestamp().toString(),
            )
        )
        val encInfo2 = devInfo2.encryptAttributes(userKey).getOrThrow()
        CommandTest(
            "device.2",
            SessionState(
                ClientRequest(
                    "DEVICE", mutableMapOf(
                        "Device-ID" to devid2.toString(),
                        "Device-Key" to dev2pair.pubKey.toString(),
                        "Key-Hash" to dev2pair.getPublicHash().getOrThrow().toString(),
                        "Device-Info" to encInfo2.toString(),
                    )
                ), env.adminProfile.wid,
                LoginState.AwaitingDeviceID
            ), ::commandDevice
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            Thread.sleep(100)

            response.assertReturnCode(105)

            val syncList = getSyncRecords(db, gAdminProfileData.waddress, 0).getOrThrow()
            assertEquals(1, syncList.size)
            val item = syncList[0]
            assertEquals(item.source, gServerDevID)

            val lfs = LocalFS.get()
            val itemHandle = lfs.entry((item.value as SyncValue.Receive).serverpath)
            assert(itemHandle.exists().getOrThrow())
            val received = Envelope.fromFile(itemHandle.getFile()).getOrThrow()
            val payload = received.open(env.adminProfile.encryption).getOrThrow()
            val devRequest = Json.decodeFromString<DeviceRequest>(payload.jsondata)
            val info = DeviceInfo.fromCryptoString(env.adminProfile.encryption, devRequest.devInfo)
                .getOrThrow()
            assertNotNull(Timestamp.fromString(info.attributes["Timestamp"]!!))
            assertEquals("myframework", info.attributes["Device Name"])
            assertEquals("Owner", info.attributes["User Name"])
            assertEquals("Windows 10 Pro", info.attributes["OS"])
            assertEquals(devid2.toString(), info.id.toString())
            assertEquals(dev2pair.pubKey.toString(), info.pubkey.toString())

            val bodyItems = devRequest.getBodyAttributes()
            assertEquals("127.0.0.1", bodyItems["IP Address"])
            assertNotNull(Timestamp.fromString(bodyItems["Timestamp"]!!))
        }.run()
        db.disconnect()
    }

    @Test
    fun loginTest() {
        val setupData = setupTest("handlers.login")
        val adminWID = RandomID.fromString(ADMIN_PROFILE_DATA["wid"])!!
        val serverKey = EncryptionKey.fromString(setupData.serverSetupData["oekey"]!!).getOrThrow()

        // We use a predetermined challenge here to make debugging easier
        val challenge = "iZghMZcgOOa|x+ZRj3QOr&0C%UuOyl@EgSR4}fFL"
        val encrypted = serverKey.encrypt(challenge.encodeToByteArray()).getOrThrow()

        // Test Case #1: Successfully log in in as admin
        val session = SessionState(
            ClientRequest(
                "LOGIN", mutableMapOf(
                    "Login-Type" to "PLAIN",
                    "Workspace-ID" to adminWID.toString(),
                    "Challenge" to encrypted.toString(),
                )
            ), null,
            LoginState.NoSession
        )
        CommandTest("login.1", session, ::commandLogin) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()

            response.assertReturnCode(100)
            assert(
                response.checkFields(
                    listOf(
                        Pair("Response", true),
                        Pair("Password-Algorithm", true),
                        Pair("Password-Salt", true),
                        Pair("Password-Parameters", true),
                    )
                )
            )
            assertEquals(challenge, response.data["Response"])
            assertEquals("argon2id", response.data["Password-Algorithm"])
            assertEquals("anXvadxtNJAYa2cUQFqKSQ", response.data["Password-Salt"])
            assertEquals("m=65536,t=2,p=1", response.data["Password-Parameters"])
        }.run()

        // Test Case #2: Login-Type missing
        CommandTest(
            "login.2",
            SessionState(
                ClientRequest(
                    "LOGIN", mutableMapOf(
                        // Leave out Login-Type
                        "Workspace-ID" to adminWID.toString(),
                        "Challenge" to encrypted.toString(),
                    )
                ), null,
                LoginState.NoSession
            ), ::commandLogin
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()

            response.assertReturnCode(400)
        }.run()

        // Test Case #3: Decryption failure
        val badChallenge = EncryptionPair.generate().getOrThrow()
            .encrypt(challenge.encodeToByteArray()).getOrThrow()

        CommandTest(
            "login.3",
            SessionState(
                ClientRequest(
                    "LOGIN", mutableMapOf(
                        "Login-Type" to "PLAIN",
                        "Workspace-ID" to adminWID.toString(),
                        "Challenge" to badChallenge.toString(),
                    )
                ), null,
                LoginState.NoSession
            ), ::commandLogin
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()

            response.assertReturnCode(306)
        }.run()

        // Test Case #4: Bad login state
        CommandTest(
            "login.4",
            SessionState(
                ClientRequest(
                    "LOGIN", mutableMapOf(
                        "Login-Type" to "PLAIN",
                        "Workspace-ID" to adminWID.toString(),
                        "Challenge" to challenge,
                    )
                ), null,
                LoginState.LoggedIn
            ), ::commandLogin
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()

            response.assertReturnCode(400)
        }.run()
    }

    @Test
    fun logoutTest() {
        val adminWID = RandomID.fromString(ADMIN_PROFILE_DATA["wid"])!!
        CommandTest(
            "logout.1",
            SessionState(
                ClientRequest("LOGOUT"), adminWID,
                LoginState.LoggedIn
            ), ::commandLogout
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()

            response.assertReturnCode(200)
        }.run()

    }

    @Test
    fun passcodeTest() {
        setupTest("handlers.passCode")
        val db = DBConn()
        setupUser(db)

        val userWID = RandomID.fromString(USER_PROFILE_DATA["wid"])!!
        val hasher = with(SHAPassword()) {
            setIterations(2)
            this
        }
        val passcode = "ignore homework every weekday"
        hasher.updateHash(passcode)
        resetPassword(db, userWID, hasher.getHash(), Timestamp().plusMinutes(10))
            ?.let { throw it }

        // Test Case #1: Auth failure
        CommandTest(
            "passcode.1",
            SessionState(
                ClientRequest(
                    "PASSCODE", mutableMapOf(
                        "Workspace-ID" to userWID.toString(),
                        "Reset-Code" to "This isn't the reg code. Really.",
                        "Password-Hash" to "This is a pretty terrible password",
                        "Password-Algorithm" to "cleartext",
                    )
                ), null,
                LoginState.NoSession
            ), ::commandPassCode
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(401)
        }.run()

        // Test Case #2: Short reset code
        CommandTest(
            "passcode.2",
            SessionState(
                ClientRequest(
                    "PASSCODE", mutableMapOf(
                        "Workspace-ID" to userWID.toString(),
                        "Reset-Code" to "foo",
                        "Password-Hash" to "This is a pretty terrible password",
                        "Password-Algorithm" to "cleartext",
                    )
                ), null,
                LoginState.NoSession
            ), ::commandPassCode
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(400)
        }.run()

        // Test Case #3: Success
        var rs = db.query("SELECT password FROM workspaces WHERE wid=?", userWID).getOrThrow()
        assert(rs.next())
        val oldhash = rs.getString("password")
        CommandTest(
            "passcode.3",
            SessionState(
                ClientRequest(
                    "PASSCODE", mutableMapOf(
                        "Workspace-ID" to userWID.toString(),
                        "Reset-Code" to passcode,
                        "Password-Hash" to "This is a pretty terrible password",
                        "Password-Algorithm" to "cleartext",
                    )
                ), null,
                LoginState.NoSession
            ), ::commandPassCode
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(200)
            rs = db.query("SELECT password FROM workspaces WHERE wid=?", userWID).getOrThrow()
            assert(rs.next())
            assertNotEquals(oldhash, rs.getString("password"))

        }.run()

        // Test Case #4: Expired
        resetPassword(db, userWID, hasher.getHash(), Timestamp().plusMinutes(-10))
            ?.let { throw it }
        CommandTest(
            "passcode.4",
            SessionState(
                ClientRequest(
                    "PASSCODE", mutableMapOf(
                        "Workspace-ID" to userWID.toString(),
                        "Reset-Code" to passcode,
                        "Password-Hash" to "This is a pretty terrible password",
                        "Password-Algorithm" to "cleartext",
                    )
                ), null,
                LoginState.NoSession
            ), ::commandPassCode
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(415)
        }.run()
        db.disconnect()
    }

    @Test
    fun passwordTest() {
        setupTest("handlers.password")
        val adminWID = RandomID.fromString(ADMIN_PROFILE_DATA["wid"])!!

        // Test Case #1: Successful password auth as admin
        CommandTest(
            "password.1",
            SessionState(
                ClientRequest(
                    "PASSWORD", mutableMapOf(
                        "Password-Hash" to ADMIN_PROFILE_DATA["password"]!!,
                    )
                ), adminWID,
                LoginState.AwaitingPassword
            ), ::commandPassword
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()

            response.assertReturnCode(100)
            assert(response.data.isEmpty())
        }.run()

        // Test Case #2: Password auth failure as admin
        CommandTest(
            "password.2",
            SessionState(
                ClientRequest(
                    "PASSWORD", mutableMapOf(
                        "Password-Hash" to "foobar",
                    )
                ), adminWID,
                LoginState.AwaitingPassword
            ), ::commandPassword
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()

            response.assertReturnCode(402)
            assert(response.data.isEmpty())
        }.run()
    }

    @Test
    fun resetPasswordTest() {
        setupTest("handlers.resetPassword")
        val db = DBConn()
        setupUser(db)

        val adminWID = RandomID.fromString(ADMIN_PROFILE_DATA["wid"])!!
        val userWID = RandomID.fromString(USER_PROFILE_DATA["wid"])!!

        // Test Case #1: Successful password reset as admin, no data specified
        CommandTest(
            "resetPassword.1",
            SessionState(
                ClientRequest(
                    "PASSWORD", mutableMapOf(
                        "Workspace-ID" to userWID.toString(),
                    )
                ), adminWID,
                LoginState.LoggedIn
            ), ::commandResetPassword
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()

            response.assertReturnCode(200)
            assert(response.checkFields(listOf(Pair("Reset-Code", true), Pair("Expires", true))))
            assert(response.data["Reset-Code"]?.length in 16..128)
            assertNotNull(Timestamp.fromString(response.data["Expires"]))
        }.run()

        // Test Case #2: Successful password reset as admin, specify all data
        val expires = Timestamp().plusDays(7)
        CommandTest(
            "resetPassword.1",
            SessionState(
                ClientRequest(
                    "PASSWORD", mutableMapOf(
                        "Workspace-ID" to userWID.toString(),
                        "Reset-Code" to "paint flower dusty rhino",
                        "Expires" to expires.toString(),
                    )
                ), adminWID,
                LoginState.LoggedIn
            ), ::commandResetPassword
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()

            response.assertReturnCode(200)
            assert(response.checkFields(listOf(Pair("Reset-Code", true), Pair("Expires", true))))
            assertEquals("paint flower dusty rhino", response.data["Reset-Code"]!!)
            assertNotNull(expires.toString(), response.data["Expires"])

            val rs = db.query("SELECT COUNT(*) FROM resetcodes WHERE wid=?", userWID)
                .getOrThrow()
            assert(rs.next())
            assertEquals(1, rs.getInt(1))
        }.run()

        // Test Case #3: User tries to set someone else's password
        CommandTest(
            "resetPassword.3",
            SessionState(
                ClientRequest(
                    "PASSWORD", mutableMapOf(
                        "Workspace-ID" to adminWID.toString(),
                    )
                ), userWID,
                LoginState.LoggedIn
            ), ::commandResetPassword
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()

            response.assertReturnCode(403)
        }.run()

        // Test Case #4: User resets their own password
        CommandTest(
            "resetPassword.4",
            SessionState(
                ClientRequest(
                    "PASSWORD", mutableMapOf(
                        "Workspace-ID" to adminWID.toString(),
                    )
                ), adminWID,
                LoginState.LoggedIn
            ), ::commandResetPassword
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()

            response.assertReturnCode(200)
        }.run()
        db.disconnect()
    }

    @Test
    fun setPasswordTest() {
        setupTest("handlers.setPassword")
        val db = DBConn()
        setupUser(db)

        val adminWID = RandomID.fromString(ADMIN_PROFILE_DATA["wid"])!!

        // Test Case #1: Unauthorized
        CommandTest(
            "setPassword.1",
            SessionState(
                ClientRequest(
                    "SETPASSWORD", mutableMapOf(
                        "Password-Hash" to "This is a pretty terrible password",
                        "NewPassword-Hash" to "This is an even worse password",
                        "NewPassword-Algorithm" to "cleartext",
                        "NewPassword-Salt" to "somethingsalty",
                        "NewPassword-Params" to "somePasswordParameters"
                    )
                ), adminWID,
                LoginState.NoSession
            ), ::commandSetPassword
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(401)
        }.run()

        // Test Case #2: Authentication failure
        CommandTest(
            "setPassword.2",
            SessionState(
                ClientRequest(
                    "SETPASSWORD", mutableMapOf(
                        "Password-Hash" to "foobar really isn't the correct password",
                        "NewPassword-Hash" to "This is an even worse password",
                        "NewPassword-Algorithm" to "cleartext",
                        "NewPassword-Salt" to "somethingsalty",
                        "NewPassword-Params" to "somePasswordParameters"
                    )
                ), adminWID,
                LoginState.LoggedIn
            ), ::commandSetPassword
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(401)
        }.run()

        // Test Case #3: Success
        var rs = db.query("SELECT password FROM workspaces WHERE wid=?", adminWID).getOrThrow()
        assert(rs.next())
        val oldhash = rs.getString("password")
        CommandTest(
            "setPassword.3",
            SessionState(
                ClientRequest(
                    "SETPASSWORD", mutableMapOf(
                        "Password-Hash" to ADMIN_PROFILE_DATA["password"]!!,
                        "NewPassword-Hash" to "This is an even worse password",
                        "NewPassword-Algorithm" to "cleartext",
                        "NewPassword-Salt" to "somethingsalty",
                        "NewPassword-Params" to "somePasswordParameters"
                    )
                ), adminWID,
                LoginState.LoggedIn
            ), ::commandSetPassword
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(200)

            rs = db.query("SELECT password FROM workspaces WHERE wid=?", adminWID).getOrThrow()
            assert(rs.next())
            assertNotEquals(oldhash, rs.getString("password"))
        }.run()
        db.disconnect()
    }
}
