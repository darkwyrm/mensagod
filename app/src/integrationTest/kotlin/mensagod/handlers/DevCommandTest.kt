package mensagod.handlers

import keznacl.*
import libkeycard.RandomID
import libmensago.ClientRequest
import libmensago.DeviceStatus
import libmensago.ServerResponse
import mensagod.*
import mensagod.dbcmds.addDevice
import mensagod.dbcmds.updateDeviceStatus
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import testsupport.ADMIN_PROFILE_DATA
import testsupport.assertReturnCode
import testsupport.gAdminProfileData
import testsupport.setupTest
import java.net.InetAddress
import java.net.Socket
import java.time.Instant

class DevCommandTest {

    @Test
    fun devKeyTest() {
        setupTest("handlers.devKey")
        val adminWID = gAdminProfileData.wid
        val devid = gAdminProfileData.devid
        val oldPair = gAdminProfileData.devpair
        val newPair = EncryptionPair.fromStrings(
            "CURVE25519:wv-Q}5&La(d-vZ?Mq@<|ad&e73)eaP%NAh{HDUo;",
            "CURVE25519:X5@Vi_4_JZ_mOPeNWKY5hOIi&1ddz#C9K`L=_&M^"
        ).getOrThrow()

        // Case #1: Successful login
        CommandTest(
            "devkey.1",
            SessionState(
                ClientRequest(
                    "DEVKEY", mutableMapOf(
                        "Device-ID" to devid.toString(),
                        "Old-Key" to oldPair.pubKey.toString(),
                        "New-Key" to newPair.pubKey.toString(),
                    )
                ),
                adminWID, LoginState.LoggedIn, devid,
            ), ::commandDevKey
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            var response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(100)

            assert(
                response.checkFields(
                    listOf(
                        Pair("Challenge", true),
                        Pair("New-Challenge", true)
                    )
                )
            )
            val oldChallenge = CryptoString.fromString(response.data["Challenge"]!!)!!
            val newChallenge = CryptoString.fromString(response.data["New-Challenge"]!!)!!
            val oldResponse = oldPair.decryptString(oldChallenge).getOrThrow()
            val newResponse = newPair.decryptString(newChallenge).getOrThrow()

            val req = ClientRequest(
                "DEVKEY", mutableMapOf(
                    "Device-ID" to devid.toString(),
                    "Response" to oldResponse,
                    "New-Response" to newResponse
                )
            )
            req.send(socket.getOutputStream())
            response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(200)

            withDB { db ->
                val rs = db.query(
                    """SELECT devkey FROM iwkspc_devices WHERE wid=? AND devid=?""",
                    adminWID, devid
                ).getOrThrow()
                assert(rs.next())
                assertEquals(newPair.pubKey.toString(), rs.getString("devkey"))
            }.onFalse { throw DatabaseException() }
        }.run()

        // Case #2: Device ID mismatch
        CommandTest(
            "devkey.2",
            SessionState(
                ClientRequest(
                    "DEVKEY", mutableMapOf(
                        "Device-ID" to devid.toString(),
                        "Old-Key" to oldPair.pubKey.toString(),
                        "New-Key" to newPair.pubKey.toString(),
                    )
                ), adminWID, LoginState.LoggedIn
            ), ::commandDevKey
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(403)
        }.run()

        // Case #3: Challenge-Response failure
        CommandTest(
            "devkey.3",
            SessionState(
                ClientRequest(
                    "DEVKEY", mutableMapOf(
                        "Device-ID" to devid.toString(),
                        "Old-Key" to oldPair.pubKey.toString(),
                        "New-Key" to newPair.pubKey.toString(),
                    )
                ),
                adminWID, LoginState.LoggedIn, devid,
            ), ::commandDevKey
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            var response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(100)

            val req = ClientRequest(
                "DEVKEY", mutableMapOf(
                    "Device-ID" to devid.toString(),
                    "Response" to "foo",
                    "New-Response" to "bar",
                )
            )
            req.send(socket.getOutputStream())
            response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(401)
        }.run()
    }

    @Test
    fun getDeviceStatusTest() {
        setupTest("handlers.getDeviceStatus")

        val adminWID = gAdminProfileData.wid
        val devpair = gAdminProfileData.devpair
        val fakeInfo = CryptoString.fromString("XSALSA20:myfakedevInfoYAY")!!

        // Loop-based test cases. Includes all status except DeviceStatus.Unregistered
        val devList = listOf(
            // Approved device
            Pair(
                RandomID.fromString("6c1a461f-e459-49eb-a7e4-46fba0d84722")!!,
                DeviceStatus.Approved
            ),

            // Blocked device
            Pair(
                RandomID.fromString("3955b8ee-01e2-4a99-bbec-8ec70bd1de05")!!,
                DeviceStatus.Pending
            ),

            // Pending device
            Pair(
                RandomID.fromString("adc5bc01-d515-43de-9e95-53464c3953ef")!!,
                DeviceStatus.Pending
            ),

            // Registered device
            Pair(gAdminProfileData.devid, DeviceStatus.Registered),
        )

        val db = DBConn()
        devList.forEach {
            db.execute(
                "INSERT INTO iwkspc_devices(wid,devid,devkey,devinfo,lastlogin,status) " +
                        "VALUES(?,?,?,?,?,?)",
                adminWID,
                it.first,
                devpair.pubKey,
                fakeInfo,
                Instant.now(),  // The lastlogin value doesn't matter in this case
                it.second
            ).getOrThrow()
        }

        // Test each of the loop-based cases
        devList.forEach {
            CommandTest(
                "getDeviceStatus.${it.second}}",
                SessionState(
                    ClientRequest(
                        "GETDEVICESTATUS",
                        mutableMapOf(
                            "Workspace-ID" to gAdminProfileData.wid.toString(),
                            "Device-ID" to it.first.toString()
                        ),
                    ), adminWID, LoginState.NoSession
                ), ::commandGetDeviceStatus
            ) { port ->
                val socket = Socket(InetAddress.getByName("localhost"), port)
                val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
                if (!response.data.containsKey("Device-Status"))
                    throw MissingDataException(
                        "Device-Status field missing for case ${it.second}"
                    )
                if (DeviceStatus.fromString(response.data["Device-Status"]!!) != it.second)
                    throw BadValueException(
                        "Expected ${it.second} for ${it.first}, got ${response.data["Device-Status"]}"
                    )
            }.run()
        }

        CommandTest(
            "getDeviceStatus.unregistered",
            SessionState(
                ClientRequest(
                    "GETDEVICESTATUS",
                    mutableMapOf(
                        "Workspace-ID" to gAdminProfileData.wid.toString(),
                        "Device-ID" to "608ec80c-e73c-45c1-9335-a501148fd3fb"
                    ),
                ), adminWID, LoginState.NoSession
            ), ::commandGetDeviceStatus
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            if (!response.data.containsKey("Device-Status"))
                throw MissingDataException(
                    "Device-Status field missing for unregistered device case"
                )
            if (DeviceStatus.fromString(response.data["Device-Status"]!!) != DeviceStatus.NotRegistered)
                throw BadValueException(
                    "Bad device status for unregistered case, got ${response.data["Device-Status"]}"
                )
        }.run()

    }

    @Test
    fun getDeviceInfoTest() {
        setupTest("handlers.getDeviceInfo")
        val adminWID = gAdminProfileData.wid
        val devid = gAdminProfileData.devid
        val devid2 = RandomID.fromString("61ae62b5-28f0-4b44-9eb4-8f81b761ad18")!!
        val devPair2 = EncryptionPair.fromStrings(
            "CURVE25519:wv-Q}5&La(d-vZ?Mq@<|ad&e73)eaP%NAh{HDUo;",
            "CURVE25519:X5@Vi_4_JZ_mOPeNWKY5hOIi&1ddz#C9K`L=_&M^"
        ).getOrThrow()
        val fakeInfo = CryptoString.fromString("XSALSA20:myfakedevInfoYAY")!!

        val db = DBConn()
        db.execute(
            "INSERT INTO iwkspc_devices(wid,devid,devkey,devinfo,lastlogin,status) " +
                    "VALUES(?,?,?,?,?,?)",
            adminWID,
            devid2,
            devPair2.pubKey,
            fakeInfo,
            Instant.now(),
            "registered"
        ).getOrThrow()

        // Case #1: Successful get for 1 device
        CommandTest(
            "getDeviceInfo.1",
            SessionState(
                ClientRequest(
                    "GETDEVICEINFO", mutableMapOf(
                        "Device-ID" to ADMIN_PROFILE_DATA["devid"]!!,
                    )
                ), adminWID, LoginState.LoggedIn
            ), ::commandGetDeviceInfo
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(200)
            assert(response.data.containsKey("Device-Info"))

            val rs = db.query(
                """SELECT devid,devinfo FROM iwkspc_devices WHERE wid=? AND devid=?""",
                adminWID,
                devid
            ).getOrThrow()
            assert(rs.next())
            assertEquals(devid.toString(), rs.getString("devid"))
            assert(rs.getString("devinfo").isNotEmpty())
        }.run()

        // Case #2: Successful get for multiple devices
        CommandTest(
            "getDeviceInfo.2",
            SessionState(
                ClientRequest(
                    "GETDEVICEINFO", mutableMapOf()
                ), adminWID, LoginState.LoggedIn
            ), ::commandGetDeviceInfo
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(200)
            assert(response.data.containsKey("Devices"))

            val devices = response.data["Devices"]!!.split(",")
            assertEquals(devid.toString(), devices[0])
            assertEquals(devid2.toString(), devices[1])

            assertEquals("XSALSA20:ABCDEFG1234567890", response.data[devid.toString()])
            assertEquals(fakeInfo.toString(), response.data[devid2.toString()])

        }.run()

        // Case #3: Device not found
        CommandTest(
            "getDeviceInfo.3",
            SessionState(
                ClientRequest(
                    "GETDEVICEINFO", mutableMapOf(
                        "Device-ID" to "4171c1e9-fab2-47bf-b383-f2342282b301",
                    )
                ), adminWID, LoginState.LoggedIn
            ), ::commandGetDeviceInfo
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(404)
        }.run()

        db.disconnect()
    }

    @Test
    fun newDeviceTest() {
        setupTest("handlers_newdevice")
        val adminWID = gAdminProfileData.wid
        val devid = RandomID.fromString("61ae62b5-28f0-4b44-9eb4-8f81b761ad18")!!
        val fakeKey = CryptoString.fromString("XSALSA20:myfakedevkeyLOL")!!
        val fakeInfo = CryptoString.fromString("XSALSA20:myfakedevInfoYAY")!!

        val db = DBConn()
        addDevice(db, adminWID, devid, fakeKey, fakeInfo, DeviceStatus.Pending)?.let { throw it }

        // Test Case #1: Successful execution
        CommandTest(
            "newdevice.1",
            SessionState(
                ClientRequest(
                    "NEWDEVICE", mutableMapOf(
                        "Device-ID" to devid.toString(),
                        "Action" to "approve",
                        "Key-Info" to "XSALSA20:abcdefg1234567890",
                    )
                ), adminWID, LoginState.LoggedIn
            ), ::commandNewDevice
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(200)
        }.run()

        // Test Case #2: Already approved
        CommandTest(
            "newdevice.2",
            SessionState(
                ClientRequest(
                    "NEWDEVICE", mutableMapOf(
                        "Device-ID" to devid.toString(),
                        "Action" to "approve",
                        "Key-Info" to "XSALSA20:abcdefg1234567890",
                    )
                ), adminWID, LoginState.LoggedIn
            ), ::commandNewDevice
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(203)
        }.run()

        // Test Case #3: Already registered
        updateDeviceStatus(db, adminWID, devid, DeviceStatus.Registered)?.let { throw it }
        CommandTest(
            "newdevice.3",
            SessionState(
                ClientRequest(
                    "NEWDEVICE", mutableMapOf(
                        "Device-ID" to devid.toString(),
                        "Action" to "approve",
                        "Key-Info" to "XSALSA20:abcdefg1234567890",
                    )
                ), adminWID, LoginState.LoggedIn
            ), ::commandNewDevice
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(201)
        }.run()

        // Test Case #4: Error for a blocked device
        updateDeviceStatus(db, adminWID, devid, DeviceStatus.Blocked)?.let { throw it }
        CommandTest(
            "newdevice.4",
            SessionState(
                ClientRequest(
                    "NEWDEVICE", mutableMapOf(
                        "Device-ID" to devid.toString(),
                        "Action" to "approve",
                        "Key-Info" to "XSALSA20:abcdefg1234567890",
                    )
                ), adminWID, LoginState.LoggedIn
            ), ::commandNewDevice
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(403)
        }.run()
        db.disconnect()
    }

    @Test
    fun removeDeviceTest() {
        setupTest("handlers.removeDevice")
        val adminWID = gAdminProfileData.wid
        val devid = gAdminProfileData.devid
        val devid2 = RandomID.fromString("61ae62b5-28f0-4b44-9eb4-8f81b761ad18")!!
        val devPair2 = EncryptionPair.fromStrings(
            "CURVE25519:wv-Q}5&La(d-vZ?Mq@<|ad&e73)eaP%NAh{HDUo;",
            "CURVE25519:X5@Vi_4_JZ_mOPeNWKY5hOIi&1ddz#C9K`L=_&M^"
        ).getOrThrow()
        val fakeInfo = CryptoString.fromString("XSALSA20:myfakedevInfoYAY")!!

        val db = DBConn()
        db.execute(
            "INSERT INTO iwkspc_devices(wid,devid,devkey,devinfo,lastlogin,status) " +
                    "VALUES(?,?,?,?,?,?)",
            adminWID,
            devid2,
            devPair2.pubKey,
            fakeInfo,
            Instant.now(),
            "registered"
        ).getOrThrow()

        // Case #1: Success
        CommandTest(
            "removedevice.1",
            SessionState(
                ClientRequest(
                    "REMOVEDEVICE", mutableMapOf(
                        "Device-ID" to devid2.toString(),
                    )
                ),
                adminWID, LoginState.LoggedIn, devid,
            ), ::commandRemoveDevice
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(200)

            val rs = db.query(
                """SELECT devid FROM iwkspc_devices WHERE wid=? AND devid=?""",
                adminWID, devid2
            ).getOrThrow()
            assert(!rs.next())
        }.run()
        db.disconnect()

        // Because of how the test itself is set up, we can't test connection termination if the
        // current device is removed. That will have to be tested via a client integration test
    }

    @Test
    fun setDeviceInfoTest() {
        setupTest("handlers.setDeviceInfo")
        val adminWID = gAdminProfileData.wid
        val devid = gAdminProfileData.devid
        val fakeInfo = "XSALSA20:myNewFakeInfoWOOT"

        // Case #1: Success
        CommandTest(
            "setdeviceinfo.1",
            SessionState(
                ClientRequest("SETDEVICEINFO", mutableMapOf("Device-Info" to fakeInfo)),
                adminWID, LoginState.LoggedIn, devid,
            ), ::commandSetDeviceInfo
        ) { port ->
            val socket = Socket(InetAddress.getByName("localhost"), port)
            val response = ServerResponse.receive(socket.getInputStream()).getOrThrow()
            response.assertReturnCode(200)

            withDB { db ->
                val rs = db.query(
                    """SELECT devinfo FROM iwkspc_devices WHERE wid=? AND devid=?""",
                    adminWID, devid
                ).getOrThrow()
                assert(rs.next())
                assertEquals(fakeInfo, rs.getString("devinfo"))
            }.onFalse { throw DatabaseException() }
        }.run()

        // This call only really needs 1 test because it's so simple and shouldn't fail unless
        // there is a problem with the database itself.
    }
}
