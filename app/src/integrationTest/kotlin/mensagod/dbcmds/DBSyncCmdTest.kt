package mensagod.dbcmds

import keznacl.BadValueException
import libkeycard.RandomID
import libmensago.SyncItem
import libmensago.SyncOp
import libmensago.SyncValue
import mensagod.DBConn
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import testsupport.ADMIN_PROFILE_DATA
import testsupport.gAdminProfileData
import testsupport.setupTest
import testsupport.setupUser
import java.time.Instant
import java.util.*

class DBSyncCmdTest {

    @Test
    fun addSyncRecordTest() {
        setupTest("dbcmds.addSyncRecordTest")
        val db = DBConn()
        setupUser(db)

        val adminWID = RandomID.fromString(ADMIN_PROFILE_DATA["wid"])!!
        val devid = RandomID.fromString(ADMIN_PROFILE_DATA["devid"])!!
        val unixTime = 12345678L
        val testFileName =
            "${Instant.now().epochSecond}.1024.${UUID.randomUUID().toString().lowercase()}"

        val createID = RandomID.fromString("6dce4c07-dfbe-48ff-bc1a-cf3d2c33dd70")!!
        val upvalue: SyncValue =
            SyncValue.Create.fromRaw("/ $adminWID $testFileName").getOrThrow()
        addSyncRecord(
            db,
            adminWID,
            SyncItem(
                createID,
                SyncOp.Create,
                gAdminProfileData.waddress,
                upvalue,
                unixTime,
                devid
            )
        )?.let { throw it }

        val rs = db.query("SELECT * FROM updates WHERE rid=?", createID).getOrThrow()
        assert(rs.next())
        assertEquals(adminWID.toString(), rs.getString("wid"))
        assertEquals(SyncOp.Create.toString(), rs.getString("update_type"))
        assertEquals("/ $adminWID $testFileName", rs.getString("update_data"))
        rs.getString("unixtime").toLong()
        assertEquals(devid.toString(), rs.getString("devid"))

        db.disconnect()
    }

    @Test
    fun getCountSyncRecordTest() {
        setupTest("dbcmds.getCountSync")
        val db = DBConn()
        setupUser(db)

        val adminWID = RandomID.fromString(ADMIN_PROFILE_DATA["wid"])!!
        val devid = RandomID.fromString(ADMIN_PROFILE_DATA["devid"])!!
        val unixtime = 1_700_000_000L

        val recID1 = RandomID.fromString("11111111-1111-1111-1111-111111111111")!!
        var testFileName = "$unixtime.1024.${UUID.randomUUID().toString().lowercase()}"
        var createValue = SyncValue.Create.fromRaw("/ $adminWID $testFileName").getOrThrow()
        addSyncRecord(
            db, adminWID, SyncItem(
                recID1, SyncOp.Create, gAdminProfileData.waddress, createValue, unixtime,
                devid
            )
        )?.let { throw it }

        val recID2 = RandomID.fromString("22222222-2222-2222-2222-222222222222")!!
        testFileName = "$unixtime.1024.${UUID.randomUUID().toString().lowercase()}"
        createValue = SyncValue.Create.fromRaw("/ $adminWID $testFileName").getOrThrow()
        addSyncRecord(
            db, adminWID, SyncItem(
                recID2, SyncOp.Create, gAdminProfileData.waddress, createValue,
                unixtime + 10, devid
            )
        )?.let { throw it }

        val recID3 = RandomID.fromString("33333333-3333-3333-3333-333333333333")!!
        testFileName = "$unixtime.1024.${UUID.randomUUID().toString().lowercase()}"
        createValue = SyncValue.Create.fromRaw("/ $adminWID $testFileName").getOrThrow()
        addSyncRecord(
            db, adminWID, SyncItem(
                recID3, SyncOp.Create, gAdminProfileData.waddress, createValue,
                unixtime + 20, devid
            )
        )?.let { throw it }

        val fullList = getSyncRecords(db, gAdminProfileData.waddress, unixtime - 1)
            .getOrThrow()
        if (fullList.size != 3) {
            println(fullList.joinToString("\n"))
            throw BadValueException()
        }
        val emptyList = getSyncRecords(db, gAdminProfileData.waddress, unixtime + 100)
            .getOrThrow()
        if (emptyList.isNotEmpty()) {
            println(emptyList.joinToString("\n"))
            throw BadValueException()
        }
        assertEquals(unixtime, fullList[0].unixtime)
        assertEquals(unixtime + 20, fullList[2].unixtime.toLong())
        assert(
            getSyncRecords(
                db,
                gAdminProfileData.waddress,
                -1
            ).exceptionOrNull() is BadValueException
        )

        assertEquals(3, countSyncRecords(db, adminWID, unixtime).getOrThrow())
        assertEquals(2, countSyncRecords(db, adminWID, unixtime + 5).getOrThrow())
        assertEquals(0, countSyncRecords(db, adminWID, unixtime + 100).getOrThrow())
        assert(countSyncRecords(db, adminWID, -1).exceptionOrNull() is BadValueException)

        val recID4 = RandomID.fromString("44444444-4444-4444-4444-444444444444")!!
        testFileName = "$unixtime.1024.${UUID.randomUUID().toString().lowercase()}"
        createValue = SyncValue.Create.fromRaw("/ $adminWID $testFileName").getOrThrow()
        addSyncRecord(
            db, adminWID, SyncItem(
                recID4, SyncOp.Create, gAdminProfileData.waddress, createValue,
                unixtime - 600_000_000, devid
            )
        )?.let { throw it }
        assertEquals(4, countSyncRecords(db, adminWID, 0).getOrThrow())
        cullOldSyncRecords(db, unixtime - 375_000_000)
        assertEquals(3, countSyncRecords(db, adminWID, 0).getOrThrow())
        db.disconnect()
    }
}
