package mensagod

import keznacl.EmptyDataException
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import testsupport.SETUP_TEST_DATABASE
import testsupport.SETUP_TEST_FILESYSTEM
import testsupport.ServerTestEnvironment
import java.nio.file.Paths
import java.util.*
import kotlin.io.path.exists

class SetupModeHandlerTest {

    @Test
    fun checkRootTest() {
        val handler = SetupModeHandler().apply { testMode = true }
        assertFalse(handler.checkRoot())
    }

    @Test
    fun checkConfig() {
        println("setupmodehandler_checkconfig()")
        val env = ServerTestEnvironment("setupmodehandler_checkconfig")
            .provision(SETUP_TEST_FILESYSTEM)

        val handler = SetupModeHandler().apply { testMode = true }
        val config = ServerConfig()
        assert(handler.checkConfig(config) is EmptyDataException)

        val filePath = Paths.get(env.testPath, "serverconfig.toml")
        filePath.toFile().writeText(config.toString())
        assertFalse(handler.checkExistingConfigFile(filePath))

        config.setValues(
            mapOf("global.domain" to "example.com", "database.password" to "password")
        ).getOrThrow()
        handler.checkConfig(config)?.let { throw it }

        filePath.toFile().writeText(config.toString())
        assert(handler.checkExistingConfigFile(filePath))

        config["global.domain"] = "QW%)#NNQEFDO)("
        filePath.toFile().writeText(config.toString())
        assertFalse(handler.checkExistingConfigFile(filePath))
    }

    @Test
    fun testGetDataLocation() {
        // This test also covers deriveLogPath because said method depends on setting the top dir

        val env = ServerTestEnvironment("setupmodehandler_getdatalocation")
            .provision(SETUP_TEST_FILESYSTEM)
        println("setupmodehandler_getdatalocation()")
        val handler = SetupModeHandler().apply { testMode = true }
        if (platformIsWindows) {
            println("-----")
            handler.getDataLocation(answers("Invalid::*+#", "C:\\Windows\\Temp"))
                ?.let { throw it }
            assertEquals("C:\\Windows\\Temp", handler.config["global.top_dir"])
            assertEquals("C:\\Windows\\Temp\\wsp", handler.config["global.workspace_dir"])

            handler.deriveLogPath()?.let { throw it }
            assertEquals("C:\\Windows\\Temp", handler.config["global.log_dir"])

            println("-----")
            handler.getDataLocation(answers(""))?.let { throw it }
            assertEquals("C:\\ProgramData\\mensagodata", handler.config["global.top_dir"])
            assertEquals(
                "C:\\ProgramData\\mensagodata\\wsp", handler.config["global.workspace_dir"]
            )

            handler.deriveLogPath()?.let { throw it }
            assertEquals("C:\\ProgramData\\mensagodata", handler.config["global.log_dir"])

        } else {
            println("-----")
            handler.getDataLocation(answers("/\u0000", "/tmp"))
                ?.let { throw it }
            assertEquals("/tmp", handler.config["global.top_dir"])
            assertEquals("/tmp/wsp", handler.config["global.workspace_dir"])

            handler.deriveLogPath()?.let { throw it }
            assertEquals("/var/log/mensagod", handler.config["global.log_dir"])

            println("-----")
            handler.getDataLocation(answers(""))?.let { throw it }
            assertEquals("/var/mensagod", handler.config["global.top_dir"])
            assertEquals("/var/mensagod/wsp", handler.config["global.workspace_dir"])

            // Only one standard log location on POSIX, so no need to test deriveLogPath() here
        }

        // The user doesn't have to enter a path that exists, but we do expect that the parent
        // directory of said path does. This is mostly so we can verify that the path is mostly
        // sane.
        val testParentPath = Paths.get(env.testPath, "mensagod").toString()
        println("-----")
        handler.getDataLocation(answers(testParentPath))?.let { throw it }
        assertEquals(testParentPath, handler.config["global.top_dir"])
    }

    @Test
    fun getRegistrationMode() {
        println("setupmodehandler_getregistrationmode()")
        val handler = SetupModeHandler().apply { testMode = true }

        listOf("Private", "moderated", "NETWORK", "pUbLiC").forEach {
            println("-----")
            handler.getRegistrationMode(answers("foo", it))?.let { e -> throw e }
            assertEquals(it.lowercase(), handler.config["global.registration"])
        }

        println("-----")
        handler.getRegistrationMode(answers(""))?.let { throw it }
        assertEquals("private", handler.config["global.registration"])
    }

    @Test
    fun getAccountForwarding() {
        println("setupmodehandler_getaccountfwding()")
        val handler = SetupModeHandler().apply { testMode = true }

        print("-----")
        handler.getAccountForwarding(answers("foo", "n", "bar", "no"))
        assertEquals(false, handler.forwardAbuse)
        assertEquals(false, handler.forwardSupport)

        print("-----")
        handler.getAccountForwarding(answers("Y", "yes"))
        assertEquals(true, handler.forwardAbuse)
        assertEquals(true, handler.forwardSupport)

        print("-----")
        handler.getAccountForwarding(answers("", ""))
        assertEquals(true, handler.forwardAbuse)
        assertEquals(true, handler.forwardSupport)
    }

    @Test
    fun getDefaultQuota() {
        println("setupmodehandler_getdefaultquota()")
        val handler = SetupModeHandler().apply { testMode = true }

        print("-----")
        handler.getDefaultQuotaSize(answers("-10", "ABCD", "100"))?.let { throw it }
        assertEquals(100, handler.config.getInteger("global.default_quota"))

        print("-----")
        handler.getDefaultQuotaSize(answers(""))?.let { throw it }
        assertEquals(0, handler.config.getInteger("global.default_quota"))
    }

    @Test
    fun getServiceAccountInfo() {
        println("setupmodehandler_getserviceaccountinfo()")
        val handler = SetupModeHandler().apply { testMode = true }

        if (platformIsWindows) {
            print("-----")
            handler.getServiceUser(answers("foobar"))
            assertNull(handler.posixUser)

            print("-----")
            handler.getServiceGroup(answers("foobargroup"))
            assertNull(handler.posixGroup)
            return
        }

        print("-----")
        handler.getServiceUser(answers("Illegal#", "A".repeat(64), "somebody"))
        assertEquals("somebody", handler.posixUser)

        print("-----")
        handler.getServiceGroup(answers("Illegal#", "A".repeat(64), "somegroup"))
        assertEquals("somegroup", handler.posixGroup)
    }

    @Test
    fun getDBHost() {
        println("setupmodehandler_getdbhost()")
        val handler = SetupModeHandler().apply { testMode = true }

        print("-----")
        handler.getDBHost(answers("Illegal#", "localhost"))
        assertEquals("localhost", handler.config["database.host"])

        print("-----")
        handler.getDBHost(answers("127.0.0.1"))
        assertEquals("127.0.0.1", handler.config["database.host"])

        print("-----")
        handler.getDBHost(answers("::1"))
        assertEquals("::1", handler.config["database.host"])

        print("-----")
        handler.getDBHost(answers(""))
        assertEquals("localhost", handler.config["database.host"])
    }

    @Test
    fun getDBPort() {
        println("setupmodehandler_getdbport()")
        val handler = SetupModeHandler().apply { testMode = true }

        print("-----")
        handler.getDBPort(answers("-10", "ABCD", "100"))?.let { throw it }
        assertEquals(100, handler.config.getInteger("database.port"))

        print("-----")
        handler.getDBPort(answers(""))?.let { throw it }
        assertEquals(5432, handler.config.getInteger("database.port"))
    }

    @Test
    fun getDBName() {
        println("setupmodehandler_getdbname()")
        val handler = SetupModeHandler().apply { testMode = true }

        print("-----")
        handler.getDBName(answers("Illegal#", "A".repeat(64), "1more", "foobar"))
            ?.let { throw it }
        assertEquals("foobar", handler.config["database.name"])

        print("-----")
        handler.getDBName(answers(""))?.let { throw it }
        assertEquals("mensago", handler.config["database.name"])
    }

    @Test
    fun getDBUser() {
        println("setupmodehandler_getdbuser()")
        val handler = SetupModeHandler().apply { testMode = true }

        print("-----")
        handler.getDBUser(answers("Illegal#", "A".repeat(64), "1more", "foobar"))
            ?.let { throw it }
        assertEquals("foobar", handler.config["database.user"])

        print("-----")
        handler.getDBUser(answers(""))?.let { throw it }
        assertEquals("mensago", handler.config["database.user"])
    }

    @Test
    fun getDBPassword() {
        println("setupmodehandler_getdbpass()")
        val handler = SetupModeHandler().apply { testMode = true }

        print("-----")
        handler.getDBPassword(answers("TooShort", "A".repeat(75), "A".repeat(75)))
            ?.let { throw it }
        assertEquals("A".repeat(75), handler.config["database.password"])
    }

    @Test
    fun getOrgName() {
        println("setupmodehandler_getorgname()")
        val handler = SetupModeHandler().apply { testMode = true }

        print("-----")
        handler.getOrgName(answers("Ille\tgal", "B".repeat(64), " ", "ACME Widgets, Inc."))
            ?.let { throw it }
        assertEquals("ACME Widgets, Inc.", handler.orgName)

        print("-----")
        handler.getOrgName(
            answers("АCМЕ Ψіԁģетѕ, Іпс. / ❤\uFE0F\uD83D\uDD25\uD83C\uDF89€ᚦ\uD835\uDEBA")
        )?.let { throw it }
        assertEquals(
            "АCМЕ Ψіԁģетѕ, Іпс. / ❤\uFE0F\uD83D\uDD25\uD83C\uDF89€ᚦ\uD835\uDEBA",
            handler.orgName
        )
    }

    @Test
    fun getOrgDomain() {
        println("setupmodehandler_getorgdomain()")
        val handler = SetupModeHandler().apply { testMode = true }

        print("-----")
        handler.getOrgDomain(
            answers("Ille\tgal", "X".repeat(254), "", "Bad,dom", "contoso.com")
        )?.let { throw it }
        assertEquals("contoso.com", handler.config["global.domain"])
    }

    @Test
    fun getLanguages() {
        println("setupmodehandler_getlanguages()")
        val handler = SetupModeHandler().apply { testMode = true }

        print("-----")
        handler.getLanguages(answers("12", "asdf,eiro", "en,spa, fr"))
        assertEquals("en,spa,fr", handler.languages)

        print("-----")
        handler.getLanguages(answers(""))
        assertNull(handler.languages)

    }

    @Test
    fun connectToDB() {
        val env = ServerTestEnvironment("setupmodehandler_connecttodb")
            .provision(SETUP_TEST_DATABASE)

        env.getDB().execute(
            """
    DO $$ DECLARE
        r RECORD;
    BEGIN
        FOR r IN (SELECT tablename FROM pg_tables WHERE schemaname = current_schema()) LOOP
            EXECUTE 'DROP TABLE IF EXISTS ' || quote_ident(r.tablename) || ' CASCADE';
        END LOOP;
    END $$;
    """
        )

        val handler = SetupModeHandler().apply { testMode = true }
        val config = ServerConfig.load().getOrThrow()
        handler.config["database.name"] = "setupmodehandler_connecttodb"
        listOf("database.port", "database.user", "database.password").forEach {
            handler.config[it] = config[it]!!
        }

        // Here we check that connectToDB() correctly detects that the database is empty.
        var db = handler.connectToDB().getOrThrow()
        assert(handler.checkDBEmpty(db, answers("n")).getOrThrow())
        db.disconnect()?.let { throw it }

        resetDB(handler.config.getDBConfig())?.let { throw it }

        // And now that the database is repopulated, assert that it correctly detected as not.
        db = handler.connectToDB().getOrThrow()
        assertFalse(handler.checkDBEmpty(db, answers("n")).getOrThrow())
        db.disconnect()?.let { throw it }
    }

    @Test
    fun runTest() {
        val env = ServerTestEnvironment("setupmodehandler_dosetup")
            .provision(SETUP_TEST_DATABASE)

        val handler = SetupModeHandler().apply { testMode = true }
        val config = ServerConfig.load().getOrThrow()
        handler.config["database.name"] = "setupmodehandler_dosetup"
        listOf("database.port", "database.user", "database.password").forEach {
            handler.config[it] = config[it]!!
        }

        val db = handler.connectToDB().getOrThrow()
        assert(handler.checkDBEmpty(db, answers("y")).getOrThrow())
        db.disconnect()?.let { throw it }

        val testAnswers: Queue<String> = LinkedList<String>().apply {
            add(Paths.get(env.testPath, "mensagodata").toString())  // mensagod data directory
            add("network")      // registration mode
            add("y")            // forward support
            add("y")            // forward abuse
            add("")             // no quota

            if (!platformIsWindows) {
                add("mensago")  // POSIX service username
                add("mensago")  // POSIX service group
            }

            add("localhost")        // hostname for database server
            add("5432")             // database port
            add("mensago")          // database name
            add("mensago")          // database user
            add("CHANGE-ME-NOW")      // database password
            add("CHANGE-ME-NOW")      // confirm database password
            add("Acme Widgets, Inc.")   // org name
            add("example.com")          // org domain
            add("eng")              // languages
        }
        handler.run(testAnswers)?.let { throw it }

        // Now we start checking to make sure the setup stuff actually did what it was supposed to.

        assert(Paths.get(env.testPath, "mensagodata").exists())
        assert(Paths.get(handler.configFileDirPath, "serverconfig.toml").exists())

        val loadedConfig = ServerConfig.load(Paths.get(handler.configFileDirPath, "serverconfig.toml"))
            .getOrThrow()
        assertEquals("network", loadedConfig["global.registration"])
        assertEquals("example.com", loadedConfig["global.domain"])
        assertEquals(0L, loadedConfig.getLong("global.default_quota"))
        assertEquals("CHANGE-ME-NOW", loadedConfig["database.password"])

        if (!platformIsWindows) {
            assertNotNull(
                POSIXUser.fromPasswdFile("/etc/passwd").getOrThrow()
                    .find { it.username == handler.posixUser })
            assertNotNull(
                POSIXGroup.fromGroupFile("/etc/group").getOrThrow()
                    .find { it.name == handler.posixGroup })
        }

        // TODO: Finish test for doSetup()
    }

    /**
     * This convenience function adds some syntactic sugar for creating new answer queues for the
     * information-gathering methods.
     */
    private fun answers(vararg args: String): Queue<String> {
        val out: Queue<String> = LinkedList()
        args.forEach { out.add(it) }
        return out
    }
}
