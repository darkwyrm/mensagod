package libmensago

import keznacl.EncryptionPair
import keznacl.SecretKey
import libkeycard.WAddress
import libmensago.resolver.KCResolver
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import testsupport.SETUP_TEST_DATABASE
import testsupport.ServerTestEnvironment
import java.time.Instant
import java.util.*

class SealedEnvelopeHeaderTest {

    private val gOneAddr = WAddress.fromString(
        "11111111-1111-1111-1111-111111111111/example.com"
    )!!
    private val gTwoAddr = WAddress.fromString(
        "22222222-2222-2222-2222-222222222222/example.com"
    )!!

    @Test
    fun basicTest() {
        // This test actually covers both EnvelopeHeader and SealedEnvelopeHeader

        val tag = EnvelopeHeader.new(AddressPair(gOneAddr, gTwoAddr)).getOrThrow()

        val recipientKey = SecretKey.generate().getOrThrow()
        val senderKey = EncryptionPair.generate().getOrThrow()
        val receiverKey = EncryptionPair.generate().getOrThrow()

        val sealed = tag.seal(recipientKey, senderKey, receiverKey).getOrThrow()

        val rInfo = sealed.decryptReceiver(receiverKey).getOrThrow()
        assertEquals(gTwoAddr, rInfo!!.to)
        assertEquals(gOneAddr.domain, rInfo.senderDom)

        val sInfo = sealed.decryptSender(senderKey).getOrThrow()
        assertEquals(gOneAddr, sInfo!!.from)
        assertEquals(gTwoAddr.domain, sInfo.recipientDom)
    }

    @Test
    fun readTest() {
        val env = ServerTestEnvironment("sealeddeliverytag_read")
            .provision(SETUP_TEST_DATABASE)

        val msg = Message(gOneAddr, gTwoAddr, ContentFormat.Text)
            .withSubject("A Message")
            .withBody("We're just testing here ")

        KCResolver.dns = FakeDNSHandler(env.serverconfig.connectToDB().getOrThrow())
        val sealed = Envelope.seal(
            env.adminProfile.encryption, msg.getAddressPair(), msg.toPayload().getOrThrow()
        ).getOrThrow()

        val filelength = sealed.toStringResult().getOrThrow().length
        val filePath = listOf(
            env.testPath,
            "${Instant.now().epochSecond}.$filelength.${UUID.randomUUID().toString().lowercase()}"
        ).toPath()
        sealed.saveWithName(filePath.toString())?.let { throw it }

        val tag = SealedEnvelopeHeader.fromFile(filePath.toFile()).getOrThrow()
        assertEquals(tag.receiver, sealed.tag.receiver)
        assertEquals(tag.sender, sealed.tag.sender)
        assertEquals(tag.payloadKey, sealed.tag.payloadKey)
        assertEquals(tag.keyHash, sealed.tag.keyHash)
        assertEquals(tag.date, sealed.tag.date)
        assertEquals(tag.version, sealed.tag.version)

        env.done()
    }
}
