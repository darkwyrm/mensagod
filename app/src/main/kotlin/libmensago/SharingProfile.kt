package libmensago

import kotlinx.serialization.Serializable

/**
 * The SharingProfile class represents the Information Sharing Profile concept described in the
 * address book spec.
 * @param name The user-visible name of the sharing profile.
 * @param description A user-facing description of the profile. This should communicate a summary of
 * what is shared and the intent behind the profile.
 * @param fields A dictionary where the keys indicate the fields included and may also have a
 * specifier stored in the value. These specifiers can be "all", "preferred", or a label and only
 * apply to contact fields which use these concepts, such as `Mail`, `Phone`, or `Social`. For
 * fields which do not need a specifier, such as `GivenName`, the value is an empty string.
 */
@Serializable
class SharingProfile(
    var name: String,

    @Suppress("MemberVisibilityCanBePrivate")
    var description: String = "",

    var fields: MutableMap<String, String> = mutableMapOf()
)
