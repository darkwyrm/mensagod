package libmensago

import keznacl.BadValueException
import keznacl.toFailure
import keznacl.toSuccess
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import libkeycard.MissingFieldException
import libkeycard.RandomID
import libkeycard.Timestamp
import libkeycard.WAddress

/**
 * The ContactMessage class is for sending messages carrying contact information. As far as delivery
 * purposes go, it's just another message. The associated factory methods requestContact,
 * approvalFromContact, and updateFromContact create messages for all stages of contact handling:
 * new contacts, approving contacts, and sending contacts updated information.
 */
@Serializable
class ContactMessage private constructor(
    var from: WAddress, var to: WAddress, var contactInfo: Contact, var body: String
) : MessageInterface {
    var subType: String = "conreq.1"

    var date: Timestamp = Timestamp()
    var subject: String = ""

    var version = 1.0f
    var type = PayloadType.ContactMessage
    var id = RandomID.generate()
    var threadID = RandomID.generate()

    override fun toPayload(): Result<Payload> {
        val rawJSON = runCatching { Json.encodeToString(this) }
            .getOrElse { return it.toFailure() }
        return Payload(PayloadType.ContactMessage, rawJSON).toSuccess()
    }

    companion object {
        fun fromPayload(p: Payload): Result<ContactMessage> {
            if (p.type != PayloadType.ContactMessage)
                return BadValueException("Wrong payload type").toFailure()

            return kotlin.runCatching { Json.decodeFromString<ContactMessage>(p.jsondata) }
        }

        fun requestContact(
            address: WAddress, info: Contact, message: String = ""
        ): Result<ContactMessage> {

            checkRequiredInfoFields(info)?.let { return it.toFailure() }

            if (info.keys == null || info.keys!!.isEmpty())
                return MissingFieldException("Keys field").toFailure()
            if (!info.keys!!.containsKey("Encryption"))
                return MissingFieldException("Keys encryption key").toFailure()
            if (!info.keys!!.containsKey("Verification"))
                return MissingFieldException("Keys verification key").toFailure()

            val from = WAddress.fromParts(info.workspace!!, info.domain!!)
            return ContactMessage(from, address, info, message).toSuccess()
        }

        fun approvalFromContact(address: WAddress, info: Contact): Result<ContactMessage> {
            return requestContact(address, info).getOrElse { return it.toFailure() }
                .apply { subType = "conreq.2" }.toSuccess()
        }

        fun infoUpdate(address: WAddress, info: Contact): Result<ContactMessage> {

            checkRequiredInfoFields(info)?.let { return it.toFailure() }
            // We don't just call requestContact() and adjust the subtype like approvalFromContact()
            // because unlike the initial request and the response, info updates don't require
            // keys to be present.

            val from = WAddress.fromParts(info.workspace!!, info.domain!!)
            return ContactMessage(from, address, info, "")
                .apply { subType = "conup" }.toSuccess()
        }
    }

    override fun sender(): WAddress {
        return from
    }

    override fun recipient(): WAddress {
        return to
    }

    override fun updateBody(str: String) {
        body = str
    }

    override fun accessBody(): String {
        return body
    }

    override fun updateSubType(str: String?) {
        subType = str
            ?: throw NullPointerException("ContactMessage instances may not have a null subtype")
    }

    @Suppress("RedundantNullableReturnType")
    override fun accessSubType(): String? {
        return subType
    }
}

private fun checkRequiredInfoFields(info: Contact): Throwable? {
    if (info.workspace == null) return MissingFieldException("Workspace ID")
    if (info.domain == null) return MissingFieldException("Domain")
    if (info.formattedName.isEmpty())
        return MissingFieldException("Formatted Name")

    return null
}
