package libmensago.commands

import keznacl.toFailure
import keznacl.toSuccess
import libkeycard.Domain
import libkeycard.RandomID
import libkeycard.WAddress
import libmensago.*

fun getStatus(conn: MConn, wid: RandomID): Result<WorkspaceStatus> {
    if (!conn.isConnected()) return NotConnectedException().toFailure()

    val req = ClientRequest("GETSTATUS", mutableMapOf("Workspace-ID" to wid.toString()))
    conn.send(req)?.let { return it.toFailure() }

    val resp = conn.receive().getOrElse { return it.toFailure() }
    if (resp.code != 200) return Result.failure(ProtocolException(resp.toStatus()))

    if (!resp.checkFields(listOf(Pair("Status", true))))
        return SchemaFailureException().toFailure()
    val status = WorkspaceStatus.fromString(resp.data["Status"]!!)
        ?: return ServerException(
            "Received bad workspace status ${resp.data["Status"]} from server"
        ).toFailure()
    return status.toSuccess()
}

fun getUpdates(conn: MConn, requestUpdatesSince: Long?): Result<List<SyncItem>> {
    if (!conn.isConnected()) return NotConnectedException().toFailure()

    val req = ClientRequest("GETUPDATES", mutableMapOf())
    req.data["Time"] = requestUpdatesSince?.toString() ?: "0"
    conn.send(req)?.let { return it.toFailure() }

    val resp = conn.receive().getOrElse { return it.toFailure() }
    if (resp.code != 200) return Result.failure(ProtocolException(resp.toStatus()))

    if (!resp.checkFields(listOf(Pair("UpdateCount", true))))
        return SchemaFailureException().toFailure()

    val updateCount = try {
        resp.data["UpdateCount"]!!.toInt()
    } catch (e: Exception) {
        return ServerException("Bad update count received from server").toFailure()
    }

    val out = mutableListOf<SyncItem>()
    for (i in 0 until updateCount) {
        if (resp.data["Update$i"] == null) {
            return Result.failure(
                ServerException("Received update message with missing update field 'Update$i'")
            )
        }

        // The first thing to do here is to validate the update item's data. Each one will have 5
        // fields separated by a pipe symbol. The first field is the update ID, and it's just an
        // integer to give some semblance of ordering to update records. The second field is the
        // update type and corresponds to the SyncOp enum class. The third field is the workspace
        // address to which the update corresponds. This is needed for updating processing on
        // clients which have multiple workspace subscriptions. The fourth field is specific to
        // update operation and is either one path or two paths separated by a colon. The last field
        // is the ID of the originating device. This prevents infinite loops in update processing.
        val parts = resp.data["Update$i"]!!.split(',').map { it.trim() }
        if (parts.size != 6) {
            return Result.failure(
                ServerException("Received bad update message 'Update$i'=${resp.data["Update$i"]!!}")
            )
        }

        val updateID = RandomID.fromString(parts[0])
            ?: return Result.failure(
                ServerException("Bad update ID '${parts[0]}' received from server")
            )

        val updateOp = SyncOp.fromString(parts[1])
            ?: return Result.failure(
                ServerException("Bad update type '${parts[1]}' received from server")
            )

        val waddress = WAddress.fromString(parts[2])
            ?: return Result.failure(
                ServerException("Bad update address '${parts[2]}' received from server")
            )

        val unixTime = try {
            parts[4].toLong()
        } catch (e: Exception) {
            return Result.failure(
                ServerException("Bad update time '${parts[4]}' received from server")
            )
        }

        val sourceDevID = RandomID.fromString(parts[5])
            ?: return Result.failure(
                ServerException("Bad update device ID '${parts[4]}' received from server")
            )

        val updateValue = SyncValue.fromString(updateOp, parts[3])
            .getOrElse { return it.toFailure() }
        out.add(
            SyncItem(updateID, updateOp, waddress, updateValue, unixTime, sourceDevID)
        )
    }

    return out.toSuccess()
}

/**
 * This command is used for sending messages up to 25MiB in size when serialized to JSON. Larger
 * messages will result in a SizeException being returned.
 *
 * @param envelopeData A string containing the output of Envelope::marshall()
 */
fun send(conn: MConn, domain: Domain, envelopeData: String): Throwable? {

    if (envelopeData.length > 0x1_900_000) return SizeException()

    return simpleServerCommand(
        conn, "SEND",
        mutableMapOf(
            "Domain" to domain.toString(),
            "Message" to envelopeData
        ),
    )
}

/**
 * This command is for sending messages of up to 2^31-1 bytes. Actual size which can be uploaded is
 * specified by the server itself.
 */
fun sendLarge(
    conn: MConn, localPath: String, domain: Domain, tempName: String = "",
    offset: Int = -1
): Throwable? {
    // Feature: SendLarge / LargeMessageSupport
    // FeatureTODO: Implement sendLarge command
    throw NotImplementedError()
}
