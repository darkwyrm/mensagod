package libmensago.commands

import keznacl.*
import libkeycard.RandomID
import libmensago.*
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.nio.file.Files
import java.nio.file.Paths

/** Copies a file to the requested directory and returns the name of the new file. */
fun copy(conn: MConn, srcFile: String, destDir: String): Result<String> {
    if (!conn.isConnected()) return NotConnectedException().toFailure()
    if (srcFile.isEmpty() || destDir.isEmpty()) return EmptyDataException().toFailure()

    val req = ClientRequest(
        "COPY", mutableMapOf(
            "SourceFile" to srcFile,
            "DestDir" to destDir,
        )
    )
    conn.send(req)?.let { return it.toFailure() }

    val resp = conn.receive().getOrElse { return it.toFailure() }
    if (resp.code != 200) return ProtocolException(resp.toStatus()).toFailure()

    if (!resp.checkFields(listOf(Pair("NewName", true))))
        return SchemaFailureException().toFailure()

    return Result.success(resp.data["NewName"]!!)
}

/** Deletes one or more server-side files */
fun delete(conn: MConn, pathList: List<String>): Throwable? {
    if (!conn.isConnected()) return NotConnectedException()
    if (pathList.isEmpty()) return null

    val args = mutableMapOf("FileCount" to pathList.size.toString())
    pathList.forEachIndexed { i, value -> args["File$i"] = value }

    val req = ClientRequest("DELETE", args)
    conn.send(req)?.let { return it }

    val resp = conn.receive().getOrElse { return it }

    return if (resp.code == 200) null else ProtocolException(resp.toStatus())
}

/**
 * Downloads a file from the server, given the complete path to the file on the server and the
 * path to the local file on disk. If the file exists on disk already, an error will be returned.
 */
fun download(conn: MConn, serverPath: MServerPath, localPath: String): Result<Long> {
    if (!conn.isConnected()) return NotConnectedException().toFailure()
    if (localPath.isEmpty()) return EmptyDataException().toFailure()

    val file = try {
        File(localPath)
    } catch (e: Exception) {
        return e.toFailure()
    }
    if (file.exists()) {
        if (file.isDirectory)
            return Result.failure(BadValueException("Local path must be a file that doesn't exist"))
        return ResourceExistsException().toFailure()
    }

    var req = ClientRequest("DOWNLOAD", mutableMapOf("Path" to serverPath.get()))
    conn.send(req)?.let { return it.toFailure() }

    val resp = conn.receive().getOrElse { return it.toFailure() }
    if (resp.code != 100) return ProtocolException(resp.toStatus()).toFailure()
    if (!resp.checkFields(listOf(Pair("Size", true))))
        return SchemaFailureException().toFailure()

    val fileSize = try {
        resp.data["Size"]!!.toLong()
    } catch (e: Exception) {
        return ServerException("Server returned a bad file size").toFailure()
    }

    // Check free disk space
    val store = if (System.getProperty("os.name").startsWith("windows", true))
        Files.getFileStore(Paths.get(localPath).root)
    else
        Files.getFileStore(Paths.get("/"))

    if (store.usableSpace <= fileSize)
        return InsufficientResourceException("Not enough free disk space to download file")
            .toFailure()

    // The client specifying the file size is a way of signalling to the server that the client
    // wants to proceed with the download.
    req = ClientRequest(
        "DOWNLOAD", mutableMapOf(
            "Path" to serverPath.get(),
            "Size" to resp.data["Size"]!!
        )
    )
    conn.send(req)?.let { return it.toFailure() }

    val buffer = ByteArray(8192)
    val ostream = try {
        FileOutputStream(file)
    } catch (e: Exception) {
        return e.toFailure()
    }
    var bytesRead = conn.read(buffer).getOrElse { return it.toFailure() }
    var totalWritten = 0L
    while (bytesRead > 0) {
        try {
            if (bytesRead == 8192)
                ostream.write(buffer)
            else
                ostream.write(buffer, 0, bytesRead)
        } catch (e: Exception) {
            return e.toFailure()
        }
        totalWritten += bytesRead
        if (totalWritten >= fileSize) break

        bytesRead = conn.read(buffer).getOrElse { return it.toFailure() }
    }

    return if (totalWritten == fileSize)
        totalWritten.toSuccess()
    else
        SizeException().toFailure()
}

/** Checks to see if a path exists on the server side */
fun exists(conn: MConn, path: String): Result<Boolean> {
    if (!conn.isConnected()) return NotConnectedException().toFailure()
    if (path.isEmpty()) return EmptyDataException().toFailure()

    val req = ClientRequest("EXISTS", mutableMapOf("Path" to path))
    conn.send(req)?.let { return it.toFailure() }

    val resp = conn.receive().getOrElse { return it.toFailure() }
    return Result.success(resp.code == 200)
}

/**
 * Returns the disk usage and limit for the current workspace as a pair of Longs. With administrator
 * permissions, another workspace may be specified.
 */
fun getQuotaInfo(conn: MConn, wid: RandomID? = null): Result<Pair<Long, Long>> {
    if (!conn.isConnected()) return NotConnectedException().toFailure()

    val req = ClientRequest("GETQUOTAINFO", mutableMapOf())
    if (wid != null)
        req.data["Workspace-ID"] = wid.toString()

    conn.send(req)?.let { return it.toFailure() }

    val resp = conn.receive().getOrElse { return it.toFailure() }
    if (resp.code != 200) return ProtocolException(resp.toStatus()).toFailure()

    if (!resp.checkFields(listOf(Pair("DiskUsage", true), Pair("QuotaSize", true))))
        return SchemaFailureException().toFailure()

    return try {
        val usage = resp.data["DiskUsage"]!!.toLong()
        val limit = resp.data["QuotaSize"]!!.toLong()
        Result.success(Pair(usage, limit))
    } catch (e: Exception) {
        ServerException("Bad quota values received from server").toFailure()
    }
}

/**
 * A keepalive command which can optionally request the number of sync updates waiting on the server
 * for the device.
 */
fun idle(conn: MConn, requestUpdatesSince: Long?): Result<Int> {
    if (!conn.isConnected()) return NotConnectedException().toFailure()

    val req = ClientRequest("IDLE", mutableMapOf())
    if (requestUpdatesSince != null) {
        if (requestUpdatesSince < 0) return BadValueException().toFailure()
        req.data["CountUpdates"] = requestUpdatesSince.toString()
    }

    conn.send(req)?.let { return it.toFailure() }

    val resp = conn.receive().getOrElse { return it.toFailure() }
    if (resp.code != 200) return ProtocolException(resp.toStatus()).toFailure()
    if (requestUpdatesSince == null) return 0.toSuccess()

    if (!resp.checkFields(listOf(Pair("UpdateCount", true))))
        return SchemaFailureException().toFailure()

    return try {
        val updateCount = resp.data["UpdateCount"]!!.toInt()
        updateCount.toSuccess()
    } catch (e: Exception) {
        ServerException("Bad update count received from server").toFailure()
    }
}

/**
 * Obtains a list of entries in a directory. If the path is empty or null, the contents of the
 * current directory are returned. If a value greater than 0 is given to epochTime, only files after
 * the specified UNIX time are returned.
 */
fun listfiles(conn: MConn, path: String? = null, epochTime: Long? = null): Result<List<String>> {
    if (!conn.isConnected()) return NotConnectedException().toFailure()

    val req = ClientRequest("LIST", mutableMapOf())
    if (path != null) req.data["Path"] = path
    if (epochTime != null) req.data["Time"] = epochTime.toString()

    conn.send(req)?.let { return it.toFailure() }

    val resp = conn.receive().getOrElse { return it.toFailure() }
    if (resp.code != 200) return ProtocolException(resp.toStatus()).toFailure()

    if (!resp.checkFields(listOf(Pair("FileCount", true), Pair("Files", true))))
        return SchemaFailureException().toFailure()

    return if (resp.data["Files"].isNullOrEmpty()) {
        listOf<String>().toSuccess()
    } else {
        val out = resp.data["Files"]!!.split(",").map { it.trim() }
        out.toSuccess()
    }
}

/**
 * Returns a list of subdirectories of the specified path or the current directory if the path is
 * null or empty.
 */
fun listdirs(conn: MConn, path: String? = null): Result<List<String>> {
    if (!conn.isConnected()) return NotConnectedException().toFailure()

    val req = ClientRequest("LISTDIRS", mutableMapOf())
    if (path != null) req.data["Path"] = path
    conn.send(req)?.let { return it.toFailure() }

    val resp = conn.receive().getOrElse { return it.toFailure() }
    if (resp.code != 200) return ProtocolException(resp.toStatus()).toFailure()

    if (!resp.checkFields(listOf(Pair("Directories", true))))
        return SchemaFailureException().toFailure()

    return if (resp.data["Directories"].isNullOrEmpty()) {
        listOf<String>().toSuccess()
    } else {
        val out = resp.data["Directories"]!!.split(",").map { it.trim() }
        out.toSuccess()
    }
}

/**
 * Creates a directory on the server side. For maximum privacy, server-side folder names must be
 * UUIDs. The path is a standard server path, e.g. "/ e95b9a22-fe2d-404f-8b19-e0282dd7a239".
 * The 'encpath' parameter is needed for cloud sync. It is a CryptoString which contains the
 * client-visible path and is encrypted with the workspace's folder encryption key. This call cannot
 * create parent directories because of folder name privacy in combination with cloud sync, and an
 * error will be returned if a parent directory doesn't exist. However, issuing a MKDIR on a path
 * that already exists will not cause errors. Instead the encrypted path will be updated with the
 * one provided.
 */
fun mkdir(conn: MConn, path: MServerPath): Throwable? {
    return simpleServerCommand(conn, "MKDIR", mutableMapOf("Path" to path.get()))
}

/** Moves the specified source file to the destination directory */
fun move(conn: MConn, srcFile: String, destDir: String): Throwable? {
    if (srcFile.isEmpty() || destDir.isEmpty()) return EmptyDataException()
    return simpleServerCommand(
        conn, "MOVE", mutableMapOf("SourceFile" to srcFile, "DestDir" to destDir)
    )
}

/**
 * Removes a directory. If the directory is not empty, this call will return a ResourceExists
 * error.
 */
fun rmdir(conn: MConn, path: MServerPath): Throwable? {
    return simpleServerCommand(conn, "RMDIR", mutableMapOf("Path" to path.get()))
}

/** Changes the working directory on the server. */
fun select(conn: MConn, path: MServerPath): Throwable? {
    return simpleServerCommand(conn, "SELECT", mutableMapOf("Path" to path.get()))
}

/** Admin-only: set the size of a workspace's disk quota. */
fun setQuota(conn: MConn, wid: RandomID, size: Long): Throwable? {
    if (!conn.isConnected()) return NotConnectedException()

    val req = ClientRequest(
        "SETQUOTA", mutableMapOf(
            "Workspace-ID" to wid.toString(),
            "Size" to size.toString(),
        )
    )

    conn.send(req).let { if (it != null) return it }

    val resp = conn.receive().getOrElse { return it }
    return if (resp.code == 200) null else ProtocolException(resp.toStatus())
}

/**
 * Uploads a local file to the server.
 */
fun upload(
    conn: MConn, localPath: String, serverPath: MServerPath, replacePath: MServerPath? = null
): Result<Pair<String, Long>> {

    if (!conn.isConnected()) return NotConnectedException().toFailure()
    if (localPath.isEmpty()) return EmptyDataException().toFailure()

    val hash = hashFile(localPath).getOrElse { return it.toFailure() }
    val file = try {
        File(localPath)
    } catch (e: Exception) {
        return e.toFailure()
    }
    if (!file.exists()) return ResourceNotFoundException().toFailure()
    if (file.isDirectory) return BadValueException("Path given is a directory").toFailure()
    val fileSize = try {
        file.length()
    } catch (e: Exception) {
        return e.toFailure()
    }

    val req = ClientRequest(
        "UPLOAD", mutableMapOf(
            "Size" to fileSize.toString(),
            "Hash" to hash.toString(),
            "Path" to serverPath.get(),
        )
    )
    if (replacePath != null)
        req.data["Replaces"] = replacePath.get()

    conn.send(req)?.let { return it.toFailure() }

    var resp = conn.receive().getOrElse { return it.toFailure() }
    if (resp.code != 100) return ProtocolException(resp.toStatus()).toFailure()

    var totalSent = 0L
    val dataBuffer = ByteArray(65532)
    val istream = runCatching { FileInputStream(file) }.getOrElse { return it.toFailure() }
    var bytesRead = runCatching { istream.read(dataBuffer) }.getOrElse { return it.toFailure() }

    while (bytesRead > 0) {
        conn.write(dataBuffer.sliceArray(0 until bytesRead))
            ?.let { return it.toFailure() }
        totalSent += bytesRead
        bytesRead = runCatching { istream.read(dataBuffer) }.getOrElse { return it.toFailure() }
    }

    resp = conn.receive().getOrElse { return it.toFailure() }
    if (resp.code != 200) return ProtocolException(resp.toStatus()).toFailure()

    if (!resp.checkFields(listOf(Pair("FileName", true))))
        return SchemaFailureException().toFailure()

    return Pair(resp.data["FileName"]!!, totalSent).toSuccess()
}

/**
 * Convenience function for implementing simple server commands. These commands have a fixed number
 * of arguments and issue a request to the server without expecting any other data back. It returns
 * an error if not connected to a server, but otherwise provides no validation. It returns null if
 * the server responds with 200 OK and a ProtocolException on any other status code.
 */
internal fun simpleServerCommand(
    conn: MConn, cmd: String,
    args: MutableMap<String, String>
): Throwable? {
    if (!conn.isConnected()) return NotConnectedException()

    val req = ClientRequest(cmd, args)
    conn.send(req)?.let { return it }
    val resp = conn.receiveWithResend(req).getOrElse { return it }

    return if (resp.code == 200) null else ProtocolException(resp.toStatus())
}
