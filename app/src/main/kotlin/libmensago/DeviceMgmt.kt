package libmensago

import keznacl.*
import kotlinx.serialization.Serializable
import libkeycard.BadFieldException
import libkeycard.RandomID
import libkeycard.Timestamp
import java.security.SecureRandom

/**
 * The DeviceInfo class is used primarily for the new device login process. It contains information
 * about the requesting device, such as the username and operating system, but it also contains
 * data needed for completing the new device login process, such as approval codes. Some of this
 * information is provided by the server, but most of it is provided by the device itself.
 *
 * The key method for this class is [encryptAttributes], as it packages and encrypts all of the
 * information needed by the user to approve or deny the new device request.
 *
 * Handling this information requires some care, as some of it could be abused -- the new device's
 * public key is part of both the encrypted and unencrypted data because the server needs access to
 * the key for device authentication purposes, but a malicious server could also pull an AITM attack
 * during the process, so the public key is also included in the encrypted data to prevent such
 * shenanigans.
 */
@Serializable
class DeviceInfo(
    val id: RandomID, val pubkey: CryptoString, val privkey: CryptoString? = null,
    var attributes: MutableMap<String, String> = mutableMapOf(),
    val approvalCode: String = getApprovalCode()
) {
    var encryptedInfo: CryptoString? = null

    override fun toString(): String =
        "DeviceInfo(\n$id,\n$pubkey,\n$privkey,\n$attributes,\n$approvalCode\n)"

    /**
     * Convenience method to update the object's attributes. Included in the attributes are the
     * device's identifying RandomID and an encryption key to use for the key package that the
     * requesting device requires.
     */
    fun collectAttributes(): Result<DeviceInfo> {
        attributes = collectInfoForDevice().getOrElse { return it.toFailure() }
        return this.toSuccess()
    }

    /**
     * Method which encrypts the attributes with the provided key and stores them in the object's
     * encryptedInfo property.
     */
    fun encryptAttributes(key: Encryptor): Result<CryptoString> {
        val tempAttrs = attributes.toMutableMap().apply {
            set("Device ID", id.toString())
            set("Device Key", pubkey.toString())
            set("Approval Code", approvalCode)
        }
        val outStr = tempAttrs.keys.joinToString("\r\n") { "$it=${tempAttrs[it]}" }
        encryptedInfo = key.encrypt(outStr.encodeToByteArray())
            .getOrElse { return it.toFailure() }
        return encryptedInfo!!.toSuccess()
    }

    /**
     * Returns an encryption keypair or null if only the public key is present
     */
    fun getKeyPair(): EncryptionPair? {
        if (privkey == null) return null
        return EncryptionPair.from(pubkey, privkey).getOrNull()
    }

    companion object {

        private fun getApprovalCode(): String {
            val rng = SecureRandom()
            val sb = StringBuilder()
            for (i in 1..3)
                sb.append(rng.nextInt(0, 9).toString())
            return sb.toString()
        }

        /**
         * Creates a new DeviceInfo object from its encrypted form.
         */
        fun fromCryptoString(key: Decryptor, data: CryptoString): Result<DeviceInfo> {
            val lines = key.decrypt(data).getOrElse { return it.toFailure() }
                .decodeToString()
                .lines()

            val attributes = mutableMapOf<String, String>()
            lines.forEach {
                val parts = it.split("=", limit = 2)
                if (parts.size != 2)
                    return BadFieldException("Bad value: $it").toFailure()
                attributes[parts[0]] = parts[1]
            }

            val tempAttrs = listOf("Device ID", "Device Key", "Approval Code")
            tempAttrs.forEach {
                if (it !in attributes)
                    return SchemaFailureException("Missing required fields").toFailure()
            }

            val id = RandomID.fromString(attributes["Device ID"])
                ?: return BadValueException("Bad Device ID").toFailure()
            val pubkey = CryptoString.fromString(attributes["Device Key"]!!)
                ?: return BadValueException("Bad Device Key").toFailure()
            val approvalCode = attributes["Approval Code"]!!
            tempAttrs.forEach { attributes.remove(it) }
            return DeviceInfo(id, pubkey, null, attributes, approvalCode).toSuccess()
        }

        /** Creates a new set of device information for the profile */
        fun generate(): Result<DeviceInfo> {

            val id = RandomID.generate()
            val keypair = EncryptionPair.generate().getOrElse { return it.toFailure() }
            val info = collectInfoForDevice().getOrElse { return it.toFailure() }
            return DeviceInfo(id, keypair.pubKey, keypair.privKey, info).toSuccess()
        }
    }
}

/**
 * collectInfoForDevice() returns a map of information about a device that is helpful for users
 * to identify the device, including the hostname, a timestamp of when the information was obtained,
 * etc.
 */
fun collectInfoForDevice(): Result<MutableMap<String, String>> {
    val out = hashMapOf(
        "Device Name" to Platform.getHostname(),
        "User Name" to Platform.getUsername().getOrElse { return it.toFailure() },
        "OS" to Platform.getOS().getOrElse { return it.toFailure() },
        "Timestamp" to Timestamp().toString(),
    )
    return out.toSuccess()
}
