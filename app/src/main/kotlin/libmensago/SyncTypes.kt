package libmensago

import keznacl.toFailure
import keznacl.toSuccess
import libkeycard.BadFieldValueException
import libkeycard.RandomID
import libkeycard.WAddress

/** The SyncOp class represents the types of operations described in update records */
enum class SyncOp {
    Create,
    Delete,
    Mkdir,
    Move,
    Receive,
    Replace,
    Rmdir,
    Rotate;

    override fun toString(): String {
        return when (this) {
            Create -> "create"
            Delete -> "delete"
            Mkdir -> "mkdir"
            Move -> "move"
            Receive -> "receive"
            Replace -> "replace"
            Rmdir -> "rmdir"
            Rotate -> "rotate"
        }
    }

    companion object {
        fun fromString(s: String): SyncOp? {
            return when (s.lowercase()) {
                "create" -> Create
                "delete" -> Delete
                "mkdir" -> Mkdir
                "move" -> Move
                "receive" -> Receive
                "replace" -> Replace
                "rmdir" -> Rmdir
                "rotate" -> Rotate
                else -> null
            }
        }
    }
}

/**
 * SyncItem objects represent a single change made in a workspace. Most of its members are
 * self-explanantory. The data field contains data specific to the operation.
 *
 * Create - path of the item created.
 * Delete - path of item deleted.
 * Copy - path to old file, full path to new file. Name of new file will be differnet from old.
 * Move - path of the file moved and the path of the folder to which it was moved.
 * Mkdir - path of the directory created.
 * Receive - path of the item received
 * Replace - path of the file replaced and the path of the file replacing it.
 * Rmdir - path of the directory removed.
 * Rotate - the path of the file containing the new key information
 *
 * As a general rule, this class serves as a bridge to a proper client-specific data type for
 * handling updates.
 */
data class SyncItem(
    val id: RandomID, val op: SyncOp, val waddress: WAddress, val value: SyncValue,
    val unixtime: Long, val source: RandomID
) {
    override fun toString(): String {
        return "$id,${op.toString().uppercase()},$waddress,$value,$unixtime,$source"
    }
}

/**
 * The SyncValue class is a collection of value subclasses for containing validated update record
 * data.
 */
sealed class SyncValue(@Suppress("unused") val op: SyncOp) {

    data class Create(val serverpath: MServerPath) : SyncValue(SyncOp.Create) {

        override fun toString(): String = serverpath.toString()

        companion object {
            fun fromRaw(s: String): Result<Create> {
                val path = MServerPath.fromString(s)
                    ?: return BadFieldValueException("Bad CREATE update path").toFailure()
                if (!path.isFile())
                    return BadFieldValueException("CREATE update path is not a file").toFailure()
                return Create(path).toSuccess()
            }
        }
    }

    data class Delete(val serverpath: MServerPath) : SyncValue(SyncOp.Delete) {

        override fun toString(): String = serverpath.toString()

        companion object {
            fun fromRaw(s: String): Result<Delete> {
                val path = MServerPath.fromString(s)
                    ?: return BadFieldValueException("Bad DELETE update path").toFailure()
                if (!path.isFile())
                    return BadFieldValueException("DELETE update path is not a file").toFailure()
                return Delete(path).toSuccess()
            }
        }
    }

    data class Move(val src: MServerPath, val dest: MServerPath) : SyncValue(SyncOp.Move) {

        override fun toString(): String = "$src:$dest"

        companion object {
            fun fromRaw(s: String): Result<Move> {
                val items = s.split(":")
                if (items.size != 2)
                    return BadFieldValueException("MOVE update path data is invalid").toFailure()
                val srcFile = MServerPath.fromString(items[0])
                    ?: return BadFieldValueException("Bad MOVE source file path").toFailure()
                if (!srcFile.isFile())
                    return BadFieldValueException("MOVE update source file path is not a file")
                        .toFailure()
                val dest = MServerPath.fromString(items[1])
                    ?: return BadFieldValueException("Bad MOVE destination path").toFailure()
                if (!dest.isDir())
                    return BadFieldValueException("MOVE update destination path is not a directory")
                        .toFailure()

                return Move(srcFile, dest).toSuccess()
            }
        }
    }

    data class Mkdir(val serverpath: MServerPath) : SyncValue(SyncOp.Mkdir) {

        override fun toString(): String = serverpath.toString()

        companion object {
            fun fromRaw(s: String): Result<Mkdir> {
                val serverpath = MServerPath.fromString(s)
                    ?: return BadFieldValueException("Bad MKDIR server path").toFailure()
                return Mkdir(serverpath).toSuccess()
            }
        }
    }

    data class Receive(val serverpath: MServerPath) : SyncValue(SyncOp.Receive) {

        override fun toString(): String = serverpath.toString()

        companion object {
            fun fromRaw(s: String): Result<Receive> {
                val path = MServerPath.fromString(s)
                    ?: return BadFieldValueException("Bad RECEIVE update path").toFailure()
                if (!path.isFile())
                    return BadFieldValueException("RECEIVE update path is not a file").toFailure()
                return Receive(path).toSuccess()
            }
        }
    }

    data class Replace(val oldfile: MServerPath, val newfile: MServerPath) :
        SyncValue(SyncOp.Replace) {

        override fun toString(): String = "$oldfile:$newfile"

        companion object {
            fun fromRaw(s: String): Result<Replace> {
                val items = s.split(":")
                if (items.size != 2)
                    return BadFieldValueException("REPLACE update path data is invalid").toFailure()
                val oldFile = MServerPath.fromString(items[0])
                    ?: return BadFieldValueException("Bad REPLACE old file path").toFailure()
                if (!oldFile.isFile())
                    return BadFieldValueException("REPLACE update old file path is not a file")
                        .toFailure()
                val newFile = MServerPath.fromString(items[1])
                    ?: return BadFieldValueException("Bad REPLACE new file path").toFailure()
                if (!newFile.isFile())
                    return BadFieldValueException("REPLACE update new file path is not a file")
                        .toFailure()

                return Replace(oldFile, newFile).toSuccess()
            }
        }
    }

    data class Rmdir(val serverpath: MServerPath) : SyncValue(SyncOp.Rmdir) {

        override fun toString(): String = serverpath.toString()

        companion object {
            fun fromRaw(s: String): Result<Rmdir> {
                val path = MServerPath.fromString(s)
                    ?: return BadFieldValueException("Bad RMDIR update path").toFailure()
                if (!path.isDir())
                    return BadFieldValueException("RMDIR update path is not a directory").toFailure()
                return Rmdir(path).toSuccess()
            }
        }
    }

    data class Rotate(val serverpath: MServerPath) : SyncValue(SyncOp.Rotate) {

        override fun toString(): String = serverpath.toString()

        companion object {
            fun fromRaw(s: String): Result<Rotate> {
                val path = MServerPath.fromString(s)
                    ?: return BadFieldValueException("Bad ROTATE update path").toFailure()
                if (!path.isFile())
                    return BadFieldValueException("ROTATE update path is not a file").toFailure()
                return Rotate(path).toSuccess()
            }
        }
    }

    companion object {
        fun fromString(op: SyncOp, s: String): Result<SyncValue> {
            return when (op) {
                SyncOp.Create -> Create.fromRaw(s)
                SyncOp.Delete -> Delete.fromRaw(s)
                SyncOp.Mkdir -> Mkdir.fromRaw(s)
                SyncOp.Move -> Move.fromRaw(s)
                SyncOp.Receive -> Receive.fromRaw(s)
                SyncOp.Replace -> Replace.fromRaw(s)
                SyncOp.Rmdir -> Rmdir.fromRaw(s)
                SyncOp.Rotate -> Rotate.fromRaw(s)
            }
        }
    }
}
