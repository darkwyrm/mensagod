package mensagod

import java.io.File

data class POSIXUser(
    val username: String, val password: String, val uid: Int, val gid: Int, val gecos: String,
    val homeDirectory: String, val shell: String
) {
    companion object {
        fun fromPasswdFile(filePath: String): Result<List<POSIXUser>> = runCatching {
            File(filePath).readLines()
                .asSequence()
                .filter { it.isNotBlank() && !it.trim().startsWith("#") }
                .map { line ->
                    val parts = line.trim().split(":")
                    POSIXUser(
                        username = parts[0], password = parts[1], uid = parts[2].toInt(), gid = parts[3].toInt(),
                        gecos = parts[4], homeDirectory = parts[5], shell = parts[6]
                    )
                }
                .toList()
        }
    }
}
