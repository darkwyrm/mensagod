package mensagod

import java.io.File

data class POSIXGroup(val name: String, val password: String, val gid: Int, val members: List<String>) {

    companion object {
        fun fromGroupFile(filePath: String): Result<List<POSIXGroup>> = runCatching {
            File(filePath).readLines()
                .asSequence()
                .filter { it.isNotBlank() && !it.trim().startsWith("#") }
                .map { line ->
                    val parts = line.trim().split(":")
                    POSIXGroup(
                        name = parts[0], password = parts[1], gid = parts[2].toInt(),
                        members = parts[3].split(",").filter { it.isNotBlank() }
                    )
                }
                .toList()
        }
    }
}
