package mensagod

import keznacl.*
import libkeycard.EntryField
import libkeycard.OrgEntry
import libkeycard.RandomID
import libkeycard.Timestamp
import libmensago.Platform
import libmensago.db.KDB
import libmensago.db.KDBConfig
import libmensago.db.PGConfig
import java.io.*
import java.lang.System.setErr
import java.net.InetAddress
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*
import java.util.prefs.Preferences.systemRoot
import java.util.regex.Pattern
import kotlin.io.path.exists
import kotlin.system.exitProcess


/**
 * Empties and resets the server's database to start from a clean slate
 *
 * @throws java.sql.SQLException on database errors
 */
fun resetDB(config: KDBConfig): Throwable? {

    val db = KDB(config).connect().getOrElse { return it }

    // Drop all tables in the database
    db.add(
        """
    DO ${'$'}${'$'} DECLARE
        r RECORD;
    BEGIN
        FOR r IN (SELECT tablename FROM pg_tables WHERE schemaname = current_schema()) LOOP
            EXECUTE 'DROP TABLE IF EXISTS ' || quote_ident(r.tablename) || ' CASCADE';
        END LOOP;
    END ${'$'}${'$'};
    """
    )

    // Create new ones

    // For logging different types of failures, such as failed username entry or a server's failure
    // to authenticate for delivery. Information stored here is to ensure that all parties which the
    // server interacts with behave themselves.
    db.add(
        """CREATE TABLE failure_log(rowid SERIAL PRIMARY KEY, type VARCHAR(16) NOT NULL,
        id VARCHAR(36), source VARCHAR(36) NOT NULL, count INTEGER,
        last_failure CHAR(20) NOT NULL, lockout_until CHAR(20));"""
    )

    // Devices registered to each individual's workspace
    db.add(
        """CREATE TABLE iwkspc_devices(rowid SERIAL PRIMARY KEY, wid CHAR(36) NOT NULL,
        devid CHAR(36) NOT NULL, devkey VARCHAR(1000) NOT NULL,
        devinfo VARCHAR(8192) NOT NULL,
        lastlogin VARCHAR(32) NOT NULL, status VARCHAR(16) NOT NULL);"""
    )

    // Information about individual workspaces
    db.add(
        """CREATE TABLE iwkspc_folders(rowid SERIAL PRIMARY KEY, wid char(36) NOT NULL,
        serverpath VARCHAR(512) NOT NULL, clientpath VARCHAR(768) NOT NULL);"""
    )

    // Stores all entries in the keycard tree
    db.add(
        """CREATE TABLE keycards(rowid SERIAL PRIMARY KEY, owner VARCHAR(292) NOT NULL,
        creationtime CHAR(20) NOT NULL, index INTEGER NOT NULL,
        entry VARCHAR(8192) NOT NULL, fingerprint VARCHAR(96) NOT NULL);"""
    )

    // Locations for key information packages needed for key exchange to new devices
    db.add(
        """CREATE TABLE keyinfo(rowid SERIAL PRIMARY KEY, wid CHAR(36) NOT NULL,
        devid CHAR(36) NOT NULL, path VARCHAR(128));"""
    )

    // Keycard information for the organization
    db.add(
        """CREATE TABLE orgkeys(rowid SERIAL PRIMARY KEY, creationtime CHAR(20) NOT NULL, 
        pubkey VARCHAR(7000), privkey VARCHAR(7000) NOT NULL, 
        purpose VARCHAR(8) NOT NULL, fingerprint VARCHAR(96) NOT NULL);"""
    )

    // Preregistration information. Entries are removed upon successful account registration.
    db.add(
        """CREATE TABLE prereg(rowid SERIAL PRIMARY KEY, wid VARCHAR(36) NOT NULL UNIQUE,
        uid VARCHAR(128), domain VARCHAR(255) NOT NULL, regcode VARCHAR(128));"""
    )

    // Disk quota tracking
    db.add(
        """CREATE TABLE quotas(rowid SERIAL PRIMARY KEY, wid CHAR(36) NOT NULL,
        usage BIGINT, quota BIGINT);"""
    )

    // Used for password resets
    db.add(
        """CREATE TABLE resetcodes(rowid SERIAL PRIMARY KEY, wid VARCHAR(36) NOT NULL UNIQUE,
        resethash VARCHAR(128) NOT NULL, expires CHAR(20) NOT NULL);"""
    )

    /*
        For logging updates made to a workspace. This table is critical to device synchronization.
        The update_data field is specific to the update type.

        Update Types
        1: CREATE. An item has been created. update_data contains the path of the item created. Note
           that this applies both to files and directories
        2: DELETE. An item has een deleted. update_data contains the path of the item created. Note
           that this applies both to files and directories. If a directory has been deleted, all of
           its contents have also been deleted, which improves performance when large directories
           go away.
        3: MOVE. An item has been moved. update_data contains two paths, the source and the
           destination. The source path contains the directory path, and in the case of a file, the
           file name. The destination contains only a folder path.
        4: ROTATE. Keys have been rotated. update_data contains the path to the encrypted key
           storage package.
    */
    db.add(
        "CREATE TABLE updates(rowid SERIAL PRIMARY KEY, rid CHAR(36) NOT NULL, " +
                "wid CHAR(36) NOT NULL, domain VARCHAR(255) NOT NULL, update_type VARCHAR(16), " +
                "update_data VARCHAR(2048), unixtime BIGINT, devid CHAR(36) NOT NULL);"
    )

    // Lookup table for all workspaces. When any workspace is created, its wid is added here.
    // userid is optional. wtype can be 'individual', 'sharing', 'group', or 'alias'
    db.add(
        """CREATE TABLE workspaces(rowid BIGSERIAL PRIMARY KEY, wid CHAR(36) NOT NULL,
            uid VARCHAR(64), domain VARCHAR(255) NOT NULL, wtype VARCHAR(32) NOT NULL,
            status VARCHAR(16) NOT NULL, password VARCHAR(256), passtype VARCHAR(32),
            salt VARCHAR(128), passparams VARCHAR(128));"""
    )

    db.executeBatch()?.let { return it }
    return db.disconnect()
}


/**
 * Handler class for an interactive command-line mode used to obtain the necessary information from
 * the user for creating the server config file.
 *
 * Note that if a configuration file already exists, its values are loaded and validated. If the
 * loaded config file has all it needs, the administrator account is registered and setup completes.
 */
class SetupModeHandler {
    private val posixPattern = Pattern.compile("^[a-z][a-z0-9-]{0,30}\$").toRegex()
    private val dbIDPattern = Pattern.compile("^[A-Za-z_][A-Za-z0-9_]{0,62}$").toRegex()

    val configFileDirPath = if (Platform.isWindows) {
        val programData = runCatching { System.getenv()["ProgramData"] }
            .getOrDefault("C:\\ProgramData")!!
        Paths.get(programData, "mensagod").toString()
    } else "/etc/mensagod"

    val config = ServerConfig()
    var testMode = false
    private val reader = BufferedReader(InputStreamReader(System.`in`))

    var posixUser: String? = null
        private set
    var posixGroup: String? = null
        private set

    private val adminWID = RandomID.generate()
    private val adminRegCode = RegCodeGenerator.getPassphrase(6)

    var forwardSupport: Boolean? = null
        private set
    private val supportWID = RandomID.generate()
    private val supportRegCode = RegCodeGenerator.getPassphrase(6)

    var forwardAbuse: Boolean? = null
        private set
    private val abuseWID = RandomID.generate()
    private val abuseRegCode = RegCodeGenerator.getPassphrase(6)

    var orgName: String? = null
        private set
    private var orgDomain: String? = null
    var languages: String? = null
        private set

    private val epair = EncryptionPair.generate().getOrElse { throw it }
    private val pspair = SigningPair.generate().getOrElse { throw it }
    private val sspair = SigningPair.generate().getOrElse { throw it }

    /**
     * Runs the interactive setup prompts. Note that a queue of strings can be passed to this
     * method to simulate user input. If not enough values are supplied, the method will start
     * prompting for console input.
     */
    fun run(testInput: Queue<String>? = null): Throwable? {

        if (!testMode) {
            checkRoot().onFalse {
                val adminStr = if (Platform.isWindows) "Administrator" else "Root"
                return UnauthorizedException("$adminStr permissions are needed for setup mode")
            }

            checkExistingConfigFile().onTrue {
                val dbconn = connectToDB().getOrElse { return it }
                doSetup(dbconn)?.let { return it }
                return dbconn.disconnect()
            }
        }

        // Step 3: Get necessary information from the user
        //
        // - location of workspace data
        // - registration type
        // - is separate abuse account desired?
        // - is separate support account desired?
        // - quota size
        // - POSIX username and group, if applicable
        // - IP address of postgres server
        // - port of postgres server
        // - database name
        // - database username
        // - database user password
        // - required keycard fields
        // - languages

        getDataLocation(testInput)?.let { return it }
        getRegistrationMode(testInput)?.let { return it }
        getAccountForwarding(testInput)
        getDefaultQuotaSize(testInput)?.let { return it }
        deriveLogPath()?.let { return it }
        getServiceUser(testInput)
        getServiceGroup(testInput)

        getDBHost(testInput)?.let { return it }
        getDBPort(testInput)?.let { return it }
        getDBName(testInput)?.let { return it }
        getDBUser(testInput)?.let { return it }
        getDBPassword(testInput)?.let { return it }

        println(
            "\n==============================================================================\n\n" +
                    "NOTE: Now it is time to enter the information used in the organization's\n" +
                    "root keycard. This information can be changed, but the original information\n" +
                    "will still be a permanent part of the organization's keycard.\n\n" +
                    "Please use care when answering."
        )

        getOrgName(testInput)?.let { return it }
        getOrgDomain(testInput)?.let { return it }
        getLanguages(testInput)

        val dbconn = connectToDB().getOrElse { return it }
        dbconn.logStatements = true

        checkDBEmpty(dbconn, testInput).getOrElse { return it }
            .onFalse { if (!testMode) exitProcess(0) }
        doSetup(dbconn)?.let { dbconn.disconnect(); return it }

        return dbconn.disconnect()
    }

    /** Convenience method to display a string prompt and return a string entered by the user */
    private fun prompt(s: String, inputQueue: Queue<String>?): String {
        print(s)
        if (!inputQueue.isNullOrEmpty()) {
            val out = inputQueue.poll().trim()
            println(out)
            return out
        }
        return runCatching { reader.readLine() ?: "" }.getOrDefault("").trim()
    }

    /** Convenience method to display a string prompt and return a string entered by the user */
    private fun promptPassword(s: String, inputQueue: Queue<String>?): String {
        print(s)
        if (!inputQueue.isNullOrEmpty()) {
            val out = inputQueue.poll()
            println("*".repeat(out.length))
            return out
        }

        return runCatching {
            System.console().readPassword(
                "Enter the password of this user (min 8 characters): "
            ).toString()
        }.getOrDefault("").trim()
    }

    /** Prompts for a binary yes/no answer, defaulting to the provided value */
    private fun promptYesNo(s: String, default: Boolean, testInput: Queue<String>? = null)
            : Boolean {
        val response = prompt(s, testInput).lowercase()
        if (response.isEmpty()) return default
        return response == "y" || response == "yes"
    }

    /** Returns true if administrator/root privileges are in place */
    fun checkRoot(): Boolean {
        val preferences = systemRoot()

        synchronized(System.err) {
            setErr(
                PrintStream(object : OutputStream() {
                    override fun write(b: Int) {}
                })
            )
            val result = runCatching {
                // On Windows, this throws a SecurityException when not admin
                preferences.put("mensagod", "something")
                preferences.remove("mensagod")

                // Under Linux, this causes a BackingStoreException unless root
                preferences.flush()

                true
            }
            setErr(System.err)
            return result.isSuccess
        }
    }

    /**
     * Checks for an existing server configuration file.
     *
     * @return True if no further processing is needed, False if no file was found, and an
     * exception if one was found, but it had problems.
     * @exception EmptyDataException If a required config parameter is missing
     * @exception BadValueException If a config parameter in the file has an invalid value
     */
    fun checkExistingConfigFile(path: Path? = null): Boolean {
        val config = ServerConfig.load(path).getOrElse { return false }
        println("Server config file found.")
        checkConfig(config)?.let { return false }
        return true
    }

    /**
     * Validates a server config
     *
     * @return True if no further processing is needed, False if no file was found, and an
     * exception if one was found, but it had problems.
     * @exception EmptyDataException If a required config parameter is missing
     * @exception BadValueException If a config parameter in the file has an invalid value
     */
    fun checkConfig(config: ServerConfig): Throwable? {
        println("Validating server config.")

        // There are 2 values which have to be set by the user: the database password and the
        // server's domain. Everything else has a default value.
        if (config.getString("database.password").isNullOrBlank())
            return EmptyDataException("Database password is required.")
        if (config.getString("database.password").isNullOrBlank())
            return EmptyDataException("Server domain is required.")

        return config.validate()?.let { BadValueException(it) }
    }

    /**
     * Gets the desired locations used by the server for storing data. The default top-level data
     * directory is C:\ProgramData\mensagodata on Windows and /var/mensagod on other platforms.
     * The workspace directory can also be set here, but because it is recommended to be directly
     * under the top-level data directory, it must be set within the config file or via an answers
     * value.
     */
    fun getDataLocation(testInput: Queue<String>? = null): Throwable? {

        val defaultDataPath = if (Platform.isWindows) {
            val programData = runCatching { System.getenv()["ProgramData"] }
                .getOrDefault("C:\\ProgramData")!!
            Paths.get(programData, "mensagodata").toString()
        } else "/var/mensagod"

        var topPath: String
        while (true) {
            val tempStr =
                prompt(
                    "Where should server-side user data be stored? [$defaultDataPath]: ",
                    testInput
                ).ifEmpty { defaultDataPath }

            try {
                val untestedPath = Paths.get(tempStr).normalize()
                if (untestedPath.exists() || untestedPath.parent.exists()) {
                    topPath = untestedPath.toString()
                    break
                }

            } catch (_: Exception) {
                println("'$tempStr' is not a valid filesystem location")
                continue
            }

            println("The path doesn't have to exist yet, but any parent directories do.")
        }
        config.setValue("global.top_dir", topPath)?.let { return it }

        val workPath = Paths.get(topPath, "wsp").toString()
        config.setValue("global.workspace_dir", workPath)?.let { return it }

        return null
    }

    /** Gets the registration mode for the server. */
    fun getRegistrationMode(testInput: Queue<String>? = null): Throwable? {
        val regModes = listOf("private", "moderated", "network", "public")

        println(
            "Registration types:\n" +
                    "  - private (default): the admin creates all accounts manually.\n" +
                    "  - moderated: anyone can ask for an account, but the admin must approve.\n" +
                    "  - network: anyone on a subnet can create a new account. By default this is\n" +
                    "        set to the local network (192.168/16, 172.16/12, 10/8).\n" +
                    "  - public: anyone with access can create a new account. Not recommended.\n"
        )
        val regMode: String
        while (true) {
            val tempStr = prompt("Registration mode [private]: ", testInput)
                .ifEmpty { "private" }
                .lowercase()
            if (regModes.contains(tempStr)) {
                regMode = tempStr
                break
            }

            println("'$tempStr' is not a valid registration mode")
        }
        config.setValue("global.registration", regMode)?.let { return it }

        return null
    }

    /** Handles setup of account forwarding for the built-in support and abuse accounts. */
    fun getAccountForwarding(testInput: Queue<String>? = null) {

        println(
            "Each instance has built-in abuse and support addresses. These can be forwarded\n" +
                    "to the admin workspace or be their own separate, distinct workspaces. Smaller\n" +
                    "environments probably will want to say 'yes' or 'y' here.\n"
        )

        forwardSupport = promptYesNo(
            "Do you want to forward support to the administrator? [Y/n]: ", true,
            testInput
        )
        forwardAbuse = promptYesNo(
            "Do you want to forward abuse to administrator? [Y/n]: ", true, testInput
        )
    }

    /**
     * Gets from the user the size, in mebibytes (MiB), of the default user quota. The default
     * value, 0, means no quota.
     */
    fun getDefaultQuotaSize(testInput: Queue<String>? = null): Throwable? {
        println(
            "\nDisk quotas can be set to a default for all users that can be changed " +
                    "individually."
        )

        while (true) {
            val tempStr = prompt(
                "\nEnter the size, in MiB, of the " +
                        "default user disk quota (0 = No quota, default): ",
                testInput
            ).ifEmpty { "0" }

            val result = runCatching { tempStr.toInt() }
            if (result.isSuccess) {
                val size = result.getOrNull()!!
                if (size < 0) {
                    println("Size may not be negative")
                    continue
                }
                return config.setValue("global.default_quota", size)
            }
            println("'$tempStr' is not a valid size in MiB")
        }
    }

    /**
     * Sets up the log file location. This is not likely to be a setting that will be changed in
     * the majority of cases, so a question for it is not asked. On Windows, the log file path is
     * the same as the config dir and /var/log/mensagod on other platforms.
     */
    fun deriveLogPath(): Throwable? {
        val logPath = if (Platform.isWindows)
            config.getString("global.top_dir")!!
        else
            "/var/log/mensagod"
        config.setValue("global.log_dir", logPath)?.let { return it }
        return null
    }

    /** Gets from the user the username associated with the Mensagod service. POSIX only. */
    fun getServiceUser(testInput: Queue<String>? = null) {
        if (Platform.isWindows) return

        val validated: String
        while (true) {
            val tempStr = prompt(
                "Enter the name of the user to run the server as. [mensago]: ", testInput
            ).ifEmpty { "mensago" }
            if (posixPattern.matches(tempStr)) {
                validated = tempStr
                break
            }

            println(
                "For the sake of compatibility with conflicting standards on usernames, please " +
                        "use one meeting the following expectactions:\n" +
                        "- 1-31 characters\n" +
                        "- The first character is a lowercase letter\n" +
                        "- The remaining characters may be lowercase letters, numbers, or hyphens\n\n" +

                        "Yes, this is annoying, but we can't control this. Sorry!"
            )
        }
        posixUser = validated
    }

    /** Gets from the group the username associated with the Mensagod service. POSIX only. */
    fun getServiceGroup(testInput: Queue<String>? = null) {
        if (Platform.isWindows) return

        val validated: String
        while (true) {
            val tempStr = prompt(
                "Enter the group of the user to run the server as. [mensago]: ", testInput
            ).ifEmpty { "mensago" }

            if (posixPattern.matches(tempStr)) {
                validated = tempStr
                break
            }

            println(
                "For the sake of compatibility with conflicting standards on groups, please " +
                        "use one meeting the following expectactions:\n" +
                        "- 1-31 characters\n" +
                        "- The first character is a lowercase letter\n" +
                        "- The remaining characters may be lowercase letters, numbers, or hyphens\n\n" +

                        "Yes, this is annoying, but we can't control this. Sorry!"
            )
        }

        posixGroup = validated
        return
    }

    /**
     * Gets the host for the database server. This can be a hostname or IP address, defaulting to
     * localhost.
     */
    fun getDBHost(testInput: Queue<String>? = null): Throwable? {
        while (true) {
            val tempStr = prompt(
                "\nEnter the hostname or IP address of the database server. [localhost]: ",
                testInput
            ).ifEmpty { "localhost" }

            val result = runCatching { InetAddress.getByName(tempStr) }
            if (result.isSuccess)
                return config.setValue("database.host", tempStr)

            println("'$tempStr' is not a valid hostname or IP address")
        }
    }

    /** Gets the port for the database port number, defaulting to 5432. */
    fun getDBPort(testInput: Queue<String>? = null): Throwable? {
        while (true) {
            val tempStr = prompt("Enter the database server port. [5432]: ", testInput)
                .ifEmpty { "5432" }

            val result = runCatching { tempStr.toInt() }
            if (result.isSuccess) {
                val size = result.getOrNull()!!
                if (size !in 1..65535) {
                    println("The database port must be a number from 1 to 65535")
                    continue
                }
                return config.setValue("database.port", size)
            }
            println("The database port must be a number from 1 to 65535")
        }
    }

    /** Gets the name of the database to be used by the mensago server, defaulting to 'mensago'. */
    fun getDBName(testInput: Queue<String>? = null): Throwable? {
        while (true) {
            val tempStr = prompt(
                "Enter the name of the database to store data. [mensago]: ", testInput
            ).ifEmpty { "mensago" }

            if (dbIDPattern.matches(tempStr))
                return config.setValue("database.name", tempStr)

            println(
                "Database names must use up to 63 letters, numbers, or " +
                        "underscores and must not begin with a number."
            )
        }
    }

    /** Gets the name of the user for the database server connection, defaulting to 'mensago'. */
    fun getDBUser(testInput: Queue<String>? = null): Throwable? {
        while (true) {
            val tempStr = prompt(
                "Enter a username which has admin privileges on this database. [mensago]: ",
                testInput
            ).ifEmpty { "mensago" }

            if (dbIDPattern.matches(tempStr))
                return config.setValue("database.user", tempStr)

            println(
                "Database usernames must use up to 63 letters, numbers, or " +
                        "underscores and must not begin with a number."
            )
        }
    }

    /** Gets the password for the database user connection. This method has no default value. */
    fun getDBPassword(testInput: Queue<String>? = null): Throwable? {
        while (true) {
            val pass = promptPassword(
                "Enter the password of this user (min 12 characters): ", testInput
            )

            if (pass.length < 12) {
                println("Password must be at least 12 characters.")
                continue
            }

            val confirm = promptPassword("Re-enter the password to confirm: ", testInput)

            if (pass == confirm) {
                if (pass.length > 64)
                    println("You're really careful, aren't you? \uD83D\uDE09")

                return config.setValue("database.password", pass)
            }

            println("Passwords do not match.")
        }
    }

    /**
     * Gets the name of the organization for its usage in the organization's keycard.
     * This method has no default value.
     */
    fun getOrgName(testInput: Queue<String>? = null): Throwable? {
        val namePattern = Pattern
            .compile("""^[^\p{C}\s][^\p{C}]{1,61}[^\p{C}\s]$""")
            .toRegex()

        while (true) {
            val tempStr = prompt("Name of organization (max 64 characters): ", testInput)
            if (tempStr.isEmpty()) {
                println("Organization name cannot be empty.")
                continue
            }

            if (namePattern.matches(tempStr)) {
                orgName = tempStr
                return null
            }
            println("Organization name may only contain Unicode printable characters.")
        }
    }

    /**
     * Gets the domain of the organization for its usage in the organization's keycard.
     * This method has no default value.
     */
    fun getOrgDomain(testInput: Queue<String>? = null): Throwable? {
        while (true) {
            val tempStr = prompt("Domain for organization (max 253 characters): ", testInput)
            if (tempStr.isEmpty()) {
                println("Organization domain cannot be empty.")
                continue
            }
            if (tempStr.length > 253) {
                println("Domains must be less than 253 characters.")
                continue
            }

            if (EntryField.domainPattern.matches(tempStr)) {
                orgDomain = tempStr
                return config.setValue("global.domain", tempStr)
            }

            println("$tempStr is an invalid domain.")
        }
    }

    /**
     * Gets the desired languages for the organization, corresponding with the same field found on
     * the organization's keycard, defaulting to none listed.
     */
    fun getLanguages(testInput: Queue<String>? = null) {
        println(
            "Listing the languages used by your organization is optional.\n\n" +

                    "Please use two- or three-letter language codes in order of preference from\n" +
                    "greatest to least and separated by a comma. You may choose up to 10 languages.\n\n" +

                    "Examples: 'en', 'eng', or 'eng,spa'\n\n" +

                    "A complete list may be found at \n" +
                    "https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes.\n"
        )
        while (true) {
            val tempStr = prompt("Language(s) [Enter to skip]: ", testInput)
            if (tempStr.isEmpty()) {
                languages = null
                return
            }

            // We reformat the string to gracefully handle whitespace introduced by the user
            val parts = tempStr.lowercase()
                .split(",")
                .map { it.trim() }
                .filter { it.isNotEmpty() }
            val reformatted = parts.joinToString(",")

            if (!EntryField.languagePattern.matches(reformatted)) {
                println("$tempStr is an invalid language string.")
                continue
            }

            if (tempStr.length > 253) {
                println("Language string cannot be larger than 253 characters")
                continue
            }

            if (parts.size > 10) {
                println("Too many languages given. Please list no more than 10.")
                continue
            }

            languages = reformatted
            return
        }
    }

    /** Attempts to connect to the database server using values in the config */
    fun connectToDB(): Result<DBConn> {
        println("Connecting to database")
        val pgconf = PGConfig(
            config.getString("database.user")
                ?: return EmptyDataException("database user").toFailure(),
            config.getString("database.password")
                ?: return EmptyDataException("database password").toFailure(),
            config.getString("database.host")
                ?: return EmptyDataException("database host").toFailure(),
            config.getInteger("database.port")
                ?: return EmptyDataException("database port").toFailure(),
            config.getString("database.name")
                ?: return EmptyDataException("database name").toFailure(),
        )
        return DBConn(pgconf).toSuccess()
    }

    /**
     * Checks to see if the database is empty. If it is not, it prompts the user to confirm that
     * it's OK to move forward and erase the database.
     */
    fun checkDBEmpty(db: DBConn, testInput: Queue<String>? = null): Result<Boolean> {
        val rs = db.query(
            "SELECT COUNT(table_name) FROM information_schema.tables WHERE table_schema = 'public';"
        ).getOrElse { return it.toFailure() }
        if (!rs.next()) return false.toSuccess()
        if (rs.getInt("count") > 0) {
            println(
                "\n================================================================================\n" +
                        "                        WARNING: the database is not empty!\n" +
                        "================================================================================\n\n" +

                        "If you continue, ALL DATA WILL BE DELETED FROM THE DATABASE, which means all\n" +
                        "users, inventory, and other information will be erased.\n"
            )

            return promptYesNo(
                "Do you want to DELETE ALL DATA and continue? [y/N]: ", false, testInput
            ).toSuccess()
        }
        return true.toSuccess()
    }

    /** Executes setup based on values obtained from the user */
    private fun doSetup(db: DBConn): Throwable? {
        // Generate and add the keys for the organization's root keycard entry

        println("Resetting database")
        resetDB(config.getDBConfig())?.let { return it }

        println("Adding organization keycard keys")
        val timestamp = Timestamp().toString()
        db.execute(
            "INSERT INTO orgkeys(creationtime, pubkey, privkey, purpose, fingerprint) " +
                    "VALUES(?,?,?,?,?)",
            timestamp, epair.pubKey, epair.privKey, "encrypt",
            epair.getPublicHash().getOrElse { return it }
        ).getOrElse { return it }

        db.execute(
            "INSERT INTO orgkeys(creationtime, pubkey, privkey, purpose, fingerprint) " +
                    "VALUES(?,?,?,?,?)",
            timestamp, pspair.pubKey, pspair.privKey, "sign",
            pspair.getPublicHash().getOrElse { return it }
        )

        db.execute(
            "INSERT INTO orgkeys(creationtime, pubkey, privkey, purpose, fingerprint) " +
                    "VALUES(?,?,?,?,?)",
            timestamp, sspair.pubKey, sspair.privKey, "altsign",
            sspair.getPublicHash().getOrElse { return it }
        )

        setupBuiltins(db)?.let { return it }
        setupKeycard(db)?.let { return it }
        setupServiceUser()?.let { return it }
        createDirectories()?.let { return it }

        val configFilePath = Paths.get(configFileDirPath, "serverconfig.toml")
        println("Saving server config file $configFilePath")
        val configData = config.toVerboseString().getOrElse { return it }
        runCatching {
            File(configFilePath.toString()).writeText(configData)
        }.getOrElse { return it }

        printFinalInfo()
        return null
    }

    /** Handles setting up the forwarding (or not) of the abuse and support built-in accounts */
    private fun setupBuiltins(db: DBConn): Throwable? {

        if (forwardAbuse!!) {
            println("Forwarding abuse account.")
            db.execute(
                "INSERT INTO workspaces(wid, uid, domain, wtype, status) VALUES(?,?,?,?,?)",
                abuseWID, "abuse", orgDomain!!, "alias", "active"
            ).getOrElse { return it }
            db.execute(
                "INSERT INTO aliases(wid, alias) VALUES(?,?)", abuseWID, "$abuseWID/$orgDomain"
            )
        } else {
            println("Provisioning abuse account.")

            db.execute(
                "INSERT INTO prereg(wid, uid, domain, regcode) VALUES(?,?,?,?)",
                abuseWID, "abuse", orgDomain!!, abuseRegCode
            ).getOrElse { return it }

            db.execute(
                "INSERT INTO workspaces(wid, uid, domain, wtype, status) VALUES(?,?,?,?,?)",
                abuseWID, "abuse", orgDomain!!, "individual", "active"
            ).getOrElse { return it }
        }

        if (forwardSupport!!) {
            println("Forwarding support account.")
            db.execute(
                "INSERT INTO workspaces(wid, uid, domain, wtype, status) VALUES(?,?,?,?,?)",
                supportWID, "support", "$supportWID/$orgDomain", "alias", "active"
            ).getOrElse { return it }
            db.execute(
                "INSERT INTO aliases(wid, alias) VALUES(?,?)",
                supportWID, "$supportWID/$orgDomain"
            )
        } else {
            println("Provisioning support account.")
            db.execute(
                "INSERT INTO prereg(wid, uid, domain, regcode) VALUES(?,?,?,?)",
                supportWID, "support", orgDomain!!, supportRegCode
            ).getOrElse { return it }

            db.execute(
                "INSERT INTO workspaces(wid, uid, domain, wtype, status) VALUES(?,?,?,?,?)",
                supportWID, "support", orgDomain!!, "individual", "active"
            ).getOrElse { return it }
        }

        return null
    }

    /** Performs the necessary operations to set up the organization's keycard */
    private fun setupKeycard(db: DBConn): Throwable? {
        println("Setting up organization keycards")
        val rootEntry = OrgEntry()
        with(rootEntry) {
            setField("Name", orgName!!)?.let { return it }
            setField("Domain", orgDomain!!)?.let { return it }
            setField("Primary-Verification-Key", pspair.pubKey.toString())
                ?.let { return it }
            setField("Secondary-Verification-Key", sspair.pubKey.toString())
                ?.let { return it }
            setField("Encryption-Key", epair.pubKey.toString())?.let { return it }
            setField("Contact-Admin", "$adminWID/$orgDomain")?.let { return it }

            if (!forwardAbuse!!)
                setField("Contact-Abuse", "$abuseWID/$orgDomain")
                    ?.let { return it }

            if (!forwardSupport!!)
                setField("Contact-Support", "$supportWID/$orgDomain")
                    ?.let { return it }

            if (languages != null && languages!!.isNotEmpty())
                setField("Language", languages!!).let { return it }

            // Now that we've added all the data to the organization's root keycard entry, let's
            // check the data and make sure the thing is compliant.

            isDataCompliant()?.let {
                println("There was a problem with the root keycard entry")
                return it
            }

            hash()?.let { return it }
            sign("Organization", pspair)?.let {
                println("There was a problem signing the root keycard entry")
                return it
            }

            isCompliant()?.let {
                println(
                    "There was a problem with the root keycard entry:\n" +
                            "${rootEntry.getFullText(null)}\n"
                )
                return it
            }
        }

        return db.execute(
            "INSERT INTO keycards(owner, creationtime, index, entry, fingerprint) " +
                    "VALUES(?1,?2,?3,?4,?5)",
            "organization", rootEntry.getFieldString("Timestamp")!!,
            rootEntry.getFieldInteger("Index")!!,
            rootEntry.getFullText(null).getOrElse { return it },
            rootEntry.getAuthString("Hash")!!
        ).exceptionOrNull()
    }

    /** POSIX-only. Sets up the system user account and group for running the server as. */
    private fun setupServiceUser(): Throwable? {
        if (Platform.isWindows) return null

        val serviceGroup = POSIXGroup.fromGroupFile("/etc/group").getOrElse { return it }
            .find { it.name == posixGroup }
        if (serviceGroup == null) {
            println("Creating system group $posixGroup")
            val info = runCommand("groupadd --system $posixGroup").getOrElse { e -> return e }
            if (info.first != 0) {
                if (testMode) {
                    println("Failed to create group: ${info.second}")
                } else {
                    println("There was an error creating the group: ${info.second}")
                    println("You may want to create the user and group manually and run setup again.")
                    return IOException(info.second)
                }
            }
        }

        val serviceUser = POSIXUser.fromPasswdFile("/etc/passwd").getOrElse { return it }
            .find { it.username == posixUser }
        if (serviceUser == null) {
            println("Creating system user $posixUser")
            val info = runCommand("useradd --system --no-user-group -s /usr/bin/false $posixUser")
                .getOrElse { e -> return e }
            if (info.first != 0) {
                if (testMode) {
                    println("Failed to create user: ${info.second}")
                } else {
                    println("There was an error creating the user: ${info.second}")
                    println("You may want to create the user and group manually and run setup again.")
                    return IOException(info.second)
                }
            }
        }

        return null
    }

    /** Creates the directories needed by the server and assigned secure permissions. */
    private fun createDirectories(): Throwable? {
        val dataDirPath = config.getString("global.top_dir")!!
        val logDirPath = config.getString("global.log_dir")!!

        println("Creating the directory for workspace data.")
        val dataDir = File(dataDirPath)
        if (!dataDir.exists())
            runCatching { dataDir.mkdirs() }.getOrElse { return it }

        println("Creating the log file directory.")
        val logDir = File(logDirPath)
        if (!logDir.exists())
            runCatching { logDir.mkdirs() }.getOrElse { return it }

        println("Creating the directory for the server config file.")
        val configFileDir = File(configFileDirPath)
        if (!configFileDir.exists())
            runCatching { configFileDir.mkdirs() }.getOrElse { return it }

        println("Setting directory ownership")
        listOf(dataDirPath, logDirPath, configFileDirPath).forEach {
            runCommand("chown $posixUser:$posixGroup $it").getOrElse { e -> return e }
        }

        return null
    }

    /**
     * Prints out the final information to the user, which includes next steps to finish setup and
     * registration information for the administrator. If the abuse and/or support accounts were set
     * up as regular accounts and not aliases, registration information for those accounts are
     * printed here, as well.
     */
    private fun printFinalInfo() {

        val configFilePath = Paths.get(configFileDirPath, "serverconfig.toml")
        println(
            "\n\n==============================================================================\n" +
                    "Basic setup is complete.\n\n" +

                    "From here, please make sure you:\n\n" +

                    "1) Review the server config file at $configFilePath .\n" +
                    "2) Make sure port ${config.getInteger("database.port")} is open on the firewall.\n" +
                    "3) Start the mensagod service and make sure it automatically starts on boot.\n" +
                    "4) Finish registration of the admin account on a device that is NOT this server.\n" +
                    "5) If you are using separate abuse or support accounts, also complete\n" +
                    "   registration for those accounts on a device that is NOT this server.\n"
        )

        println("Administrator workspace: $adminWID/$orgDomain")
        println("Administrator registration code: $adminRegCode")

        if (!forwardAbuse!!) {
            println("Abuse workspace: $abuseWID/$orgDomain")
            println("Abuse registration code: $abuseRegCode")
        }

        if (!forwardSupport!!) {
            println("Support workspace: $supportWID/$orgDomain")
            println("Support registration code: $supportRegCode")
        }
    }

    /** Convenience function for running shell commands */
    private fun runCommand(command: String): Result<Pair<Int, String>> {
        return runCatching {
            val process = ProcessBuilder(command.split(" "))
                .redirectErrorStream(true)
                .start()
            val text = process.inputStream.bufferedReader().use(BufferedReader::readText)
            return Pair(process.waitFor(), text).toSuccess()
        }
    }
}
