package mensagod

import keznacl.*
import libkeycard.MissingDataException
import libmensago.NotConnectedException
import libmensago.db.KDB
import libmensago.db.PGConfig
import java.sql.Connection
import java.sql.ResultSet

/**
 * The DBConn class is a frontend to the storage database for the application. It provides an
 * interface to the storage medium which abstracts away the specific type of database and the
 * details needed to connect to it. It also makes interaction with the Java database API a little
 * easier.
 */
class DBConn(connConfig: PGConfig? = null) {
    private val kdb = KDB(connConfig)

    init {
        connect().getOrThrow()
    }


    @Suppress("unused")
    var logConnections = kdb.logConnections

    @Suppress("unused")
    var logStatements = kdb.logStatements

    /**
     * Connects to the server with the information specified in initialize()
     *
     * @throws EmptyDataException if called before initialize()
     * @throws java.sql.SQLException if there are problems connecting to the database
     */
    fun connect(): Result<DBConn> {
        kdb.connect(dbConfig).getOrElse { return it.toFailure() }
        return this.toSuccess()
    }

    /**
     * Disconnects from the database
     *
     * @throws java.sql.SQLException if there are problems disconnecting from the database
     */
    fun disconnect(): Throwable? = kdb.disconnect()

    fun isConnected(): Boolean = kdb.isConnected()

    fun getConnection(): Connection? = kdb.getConnection()

    /**
     * Executes a query to the database
     *
     * @throws EmptyDataException if your query is empty
     * @throws NotConnectedException if not connected to the database
     * @throws BadValueException if the query placeholder count doesn't match the query argument count
     * @throws java.sql.SQLException for database problems, most likely either with your query or with the connection
     */
    fun query(q: String, vararg args: Any): Result<ResultSet> = kdb.query(q, *args)

    /**
     * Executes a single command on the database
     *
     * @throws EmptyDataException if your query is empty
     * @throws NotConnectedException if not connected to the database
     * @throws BadValueException if the query placeholder count doesn't match the query argument count
     * @throws java.sql.SQLException for database problems, most likely either with your query or with the connection
     */
    fun execute(s: String, vararg args: Any): Result<Int?> = kdb.execute(s, *args)

    /**
     * Adds a command to the internal list of handlers to be executed in batch
     *
     * @throws EmptyDataException if your query is empty
     * @throws NotConnectedException if not connected to the database
     * @throws BadValueException if the query placeholder count doesn't match the query argument count
     * @throws java.sql.SQLException for database problems, most likely either with your query or with the connection
     */
    fun add(s: String): Throwable? = kdb.add(s)

    /**
     * Runs all handlers in the internal list created with add()
     *
     * @throws EmptyDataException if your query is empty
     * @throws NotConnectedException if not connected to the database
     * @throws BadValueException if the query placeholder count doesn't match the query argument count
     * @throws java.sql.SQLException for database problems, most likely either with your query or with the connection
     */
    fun executeBatch(): Throwable? = kdb.executeBatch()

    /**
     * Checks to see if at least one row is returned for a query. It expects usage of SELECT because
     * a database may not necessarily support COUNT().
     *
     * @throws EmptyDataException if your query is empty
     * @throws NotConnectedException if not connected to the database
     * @throws BadValueException if the query placeholder count doesn't match the query argument count
     * @throws java.sql.SQLException for database problems, most likely either with your query or with the connection
     */
    fun exists(q: String, vararg args: Any): Result<Boolean> = kdb.exists(q, *args)

    companion object {
        private var dbConfig: PGConfig? = null

        /**
         * This sets up easy access to the database server so that other parts of the server can
         * just focus on connecting and interacting with the database. It also performs a
         * connection test to confirm all is well.
         *
         * @throws MissingDataException if the database password is empty or missing
         * @throws java.sql.SQLException if there are problems connecting to the database
         */
        fun initialize(config: ServerConfig): Throwable? {
            config.getString("database.password").onNullOrEmpty {
                return MissingDataException("Database password must not be empty")
            }

            val pgc = PGConfig(
                config.getString("database.user")!!,
                config.getString("database.password")!!,
                config.getString("database.host")!!,
                config.getInteger("database.port")!!,
                config.getString("database.name")!!
            )

            pgc.connect().getOrElse { return it }.close()

            dbConfig = pgc
            return null
        }
    }
}

/**
 * Block function to run short sections requiring a database connection that also return
 * a value.
 *
 * @exception DatabaseException Returned if there is a problem connecting to the database
 */
inline fun <V> withDBResult(block: (db: DBConn) -> V): Result<V> {
    val db =
        runCatching { DBConn() }.getOrElse { return Result.failure(DatabaseException()) }
    val out = block(db)
    db.disconnect()
    return Result.success(out)
}

/**
 * Block function to run short sections requiring a database connection.
 *
 * @return Indicates success connecting to the database. False indicates a problem.
 * @exception DatabaseException Returned if there is a problem connecting to the database
 */
inline fun withDB(block: (db: DBConn) -> Unit): Boolean {
    val db = runCatching { DBConn() }.getOrElse { return false }
    block(db)
    db.disconnect()
    return true
}

